﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;

public class KeyPanel : MonoBehaviour {
    public Image goldKeyImage; 
    public Image silverKeyImage; 

	void Update () {
	    if (Key.UseKey(Key.KeyType.GoldKey)) goldKeyImage.gameObject.SetActive(true);
        if (Key.UseKey(Key.KeyType.SilverKey)) silverKeyImage.gameObject.SetActive(true);
	}//Update
}//KeyPanel
