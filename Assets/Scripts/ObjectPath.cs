﻿using UnityEngine;
using System.Collections;

public class ObjectPath : MonoBehaviour {
    public Vector3[] pathPoints;
    public float movementSpeed;

    private int currentPoint;
    private Vector3 startingPoint; 
    private Vector3 target; 
    private float totalDist; 

    private void Start() {
        if (pathPoints != null) {
            currentPoint = 0;
            startingPoint = this.transform.position;
            target = startingPoint + pathPoints[currentPoint];
        }
    }//Start

    private void Update() {
        float step = Time.deltaTime * movementSpeed;
        transform.position = Vector3.MoveTowards(transform.position, target, step);

        if (transform.position == target) {
            ++currentPoint;
            if (currentPoint >= pathPoints.Length) {
                currentPoint = -1; 
                target       = startingPoint;
            } else {
                target = startingPoint + pathPoints[currentPoint];
            }
        }
    }//Update

#if UNITY_EDITOR
    private void OnDrawGizmos() {
        if (!UnityEditor.EditorApplication.isPlaying) {
            Vector3 lastPoint = this.transform.position;
            if (pathPoints != null) {
                foreach (Vector3 path in pathPoints) {
                    Gizmos.color = new Color(1f, 0.8f, 1f, 0.5f);
                    Gizmos.DrawLine(lastPoint, path + transform.position);
                    Gizmos.DrawSphere(path + transform.position, 0.2f);
                    lastPoint = path + transform.position;
                }
            }
            Gizmos.DrawLine(lastPoint, this.transform.position);
        }
    }//OnDrawGizmos
#endif
}//ObjectPath
