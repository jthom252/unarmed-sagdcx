﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
    public enum DoorBehavior {
        OpenOnRoomComplete,
        OpenOnInteracted,
        OpenWithKey
    }
    public DoorBehavior behavior = DoorBehavior.OpenOnRoomComplete; 
    public Key.KeyType keyType = Key.KeyType.GoldKey; 
    public float openTime = 1f; 
    public AudioClip openSound; 

    private bool _isOpened;
    public bool isOpened {
        get {
            return _isOpened; 
        }
    }
    private float openingTime; 
    private Interactable interactable; 

    private void Start() {
        _isOpened = false; 
        openingTime = 0f;

        if (GetComponent<Interactable>()) {
            interactable = GetComponent<Interactable>();
            interactable.Interact += OnInteract;
        }
    }//Start

    private void OnInteract() {
        if (interactable.activeState) {
            Open();
        } else {
            Close(); 
        }
    }//OnInteract

    private void Update() {
        if (openingTime <= openTime) {
            openingTime += Time.deltaTime; 
            if (_isOpened) {
                this.transform.localScale = new Vector3(1f, Mathf.Lerp(1f,0.025f,openingTime/openTime),1f);
            } else {
                this.transform.localScale = new Vector3(1f, Mathf.Lerp(0.025f,1f,openingTime / openTime), 1f);
            }
        }
    }//Update

    public void Open() {
        _isOpened = true; 
        openingTime = 0; 
    }//Open

    public void Close() {
        _isOpened = false; 
        openingTime = 0;
    }//Close

    private void OnCollisionEnter() {
        if (behavior == DoorBehavior.OpenWithKey && Key.UseKey(keyType) && !_isOpened) {
            GameHandler.PlayWorldAudio(openSound);
            Open();
        }
    }//OnCollisionEnter
}//Door
