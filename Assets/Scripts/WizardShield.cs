﻿using UnityEngine;
using System.Collections;

public class WizardShield : Shield {
    public AudioClip shatterSound; 
    public AudioClip hitSound;
    public GameObject explosionEffect; 
    private Damageable damageable; 

    private void Start() {
        if (GetComponent<Damageable>()) {
            damageable = GetComponent<Damageable>(); 
            damageable.OnHit += OnHit; 
            damageable.OnDeath += OnDeath;
        } else {
            throw new MissingComponentException("WizardShield");
        }

        GetComponent<MeshRenderer>().enabled = false; 
    }//Start

    private void OnHit() {
        GameHandler.PlayWorldAudio(hitSound);
    }//OnHit

    private void OnDeath() {
        Debug.Log("Removing Shield");
        canBlock = false;
        GameHandler.PlayWorldAudio(shatterSound);
        Explode();
        GetComponent<MeshRenderer>().enabled = false;
    }//OnDeath

    private void Explode() {
        if (explosionEffect != null) {
            GameObject explode = (GameObject)GameObject.Instantiate(
                explosionEffect,
                transform.position,
                Quaternion.identity);
            explode.transform.parent = transform.parent.parent.parent;
        }
    }//Explode

    private void OnTriggerEnter(Collider col) {
        Hazardous hazard = col.GetComponent<Hazardous>();
        if (hazard != null && hazard.friend != damageable.friend) {
            damageable.DealDamage(1);
        }

        base.OnTriggerEnter(col); 
    }//OnTriggerEnter
}//WizardShield
