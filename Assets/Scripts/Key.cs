﻿using UnityEngine;
using System.Collections;

public class Key : MonoBehaviour {
    public enum KeyType {
        GoldKey,
        SilverKey,
    }

    public struct KeyHeld {
        public bool hasGoldKey; 
        public bool hasSilverKey;
        
        public KeyHeld(bool gold = false, bool silver = false) {
            hasGoldKey = gold;
            hasSilverKey = silver;
        }
    }

    public float rotateSpeed = 0.1f; 
    public KeyType type = KeyType.GoldKey;

    public static Color goldKeyColor = new Color(1f, 0.92f, 0f); 
    public static Color silverKeyColor = new Color(0.9f,0.9f,0.9f); 
    public static Color bossKeyColor = new Color(1f,0f,0f);

    private static Key.KeyHeld keys = new Key.KeyHeld();
    public static event GameHandler.GameEvent OnKeyPickedUp;
    public static event GameHandler.GameEvent OnKeyUsed; 

    public AudioClip pickupSound; 

    private void Start() {
        this.GetComponent<Renderer>().material.color = GetKeyColor(type); 
    }//Start

	private void FixedUpdate () {
	    this.transform.Rotate(new Vector3(0f,0f,rotateSpeed)); 
	}//Update

    private void OnTriggerEnter(Collider col) {
        if (col.gameObject.layer == Top.LAYER_PLAYER) {
            AddKey(type);
            GameHandler.PlayWorldAudio(pickupSound);
            Destroy(this.gameObject);
        }
    }//OnTriggerEnter

    public static Color GetKeyColor(KeyType type) {
        switch (type) {
            case KeyType.GoldKey: return goldKeyColor;
            case KeyType.SilverKey: return silverKeyColor; 
        }

        return new Color(1f,1f,1f);
    }//GetKeyColor

    public static void AddKey(Key.KeyType type) {
        switch (type) {
            case Key.KeyType.GoldKey: keys.hasGoldKey = true; break;
            case Key.KeyType.SilverKey: keys.hasSilverKey = true; break;
        }
        if (OnKeyPickedUp != null) OnKeyPickedUp();
    }//AddKey

    public static bool UseKey(Key.KeyType type) {
        switch (type) {
            case Key.KeyType.GoldKey: 
                if (keys.hasGoldKey) {
                    return true; 
                }
                break;
            case Key.KeyType.SilverKey: 
                if (keys.hasSilverKey) {
                    return true; 
                }
                break; 

            default: return false; break; 
        }
        return false;
    }//UseKey
}//Key
