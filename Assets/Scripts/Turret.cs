﻿using UnityEngine;
using System.Collections; 

public class Turret : MonoBehaviour {
    public float[] firePattern; 
    public GameObject projectile;
    public Vector3 spawnPosition; 
    public float projectileSpeed = 1.5f;
    public bool  projectileRandomBounce = false;
    public bool    targetPlayer;
    public float spawnPositionMaxX = 1f; 
    public float spawnPositionMaxZ = 1f; 
    public bool isActivated = true; 
    public AudioClip fireSound; 

    private GameObject lastCreatedProjectile; 
    private float elapsedTime;
    private int   firePosition; 

    private Elemental.Element element;

	// Use this for initialization
	private void Start () {
        if (this.GetComponent<Interactable>()) {
            GetComponent<Interactable>().Interact += OnInteract; 
        }

        if (this.GetComponent<Damageable>()) {
            this.GetComponent<Damageable>().OnDeath += OnDeath;
        }

	    elapsedTime  = 0f;
        firePosition = 0; 

	    Elemental el = GetComponent<Elemental>();
	    if (el != null)
        {
            element = el.element;
        } else {
            element = 0;
        }
	}//Start

    private void OnDestroy() {
        if (this.GetComponent<Interactable>()) {
            GetComponent<Interactable>().Interact -= OnInteract;
        }
    }//OnDestroy 

    private void OnInteract() {
        isActivated = GetComponent<Interactable>().activeState; 
    }//OnInteract

    private void OnDeath() { 
        Destroy(gameObject);
    }//OnDeath
	
	// Update is called once per frame
	private void Update () {
        if (isActivated) {
            if (elapsedTime >= firePattern[firePosition]) {
                float xAngle, zAngle;
                Transform homingTarget = GameHandler.player.transform;

                if (!targetPlayer) {
                    xAngle = spawnPosition.x;
                    zAngle = spawnPosition.z;
                } else {
                    xAngle = (this.transform.position.x - spawnPosition.x) - homingTarget.position.x;
                    zAngle = (this.transform.position.z - spawnPosition.z) - homingTarget.position.z;
                }

                float targetRotation = Mathf.Atan2(zAngle, xAngle);
                float projectileRotation = targetRotation * 180f / Mathf.PI;

                if (targetPlayer) {
                    spawnPosition = new Vector3(Mathf.Cos(targetRotation), 0f, Mathf.Sin(targetRotation));
                }

                GameHandler.PlayWorldAudio(fireSound);
                lastCreatedProjectile = (GameObject)Instantiate(projectile,
                    gameObject.transform.position - spawnPosition + new Vector3(0f,0.5f),
                    Quaternion.Euler(new Vector3(0f, projectileRotation)));
                lastCreatedProjectile.transform.parent = this.transform.parent;
                lastCreatedProjectile.GetComponent<Projectile>().moveSpeed = projectileSpeed;
                lastCreatedProjectile.GetComponent<Projectile>().randomBounce = projectileRandomBounce;

                if (element != 0) {
                    lastCreatedProjectile.GetComponent<Elemental>().element = element;
                }

                ++firePosition;
                if (firePosition >= (firePattern.Length)) {
                    firePosition = 0;
                }

                elapsedTime = 0f;
            } else {
                elapsedTime += Time.deltaTime;
            }
        }
	}//Update

    private void OnDrawGizmos() {
        Gizmos.color = Color.red; 
        Gizmos.DrawLine(this.transform.position, this.transform.position - spawnPosition); 
        Gizmos.DrawSphere(this.transform.position - spawnPosition, 0.1f);
    }//OnDrawGizmos
}//Turret
