﻿using UnityEngine;
using System.Collections;

public class Corpse : MonoBehaviour {
    public GameObject[] gibTypes;
    public GameObject[] gibSpawnOnce; 
    public int gibsToSpawn = 10;
    public float spawnRange = 0.5f; 

    private void Start() {
        if (gibTypes != null && gibTypes.Length != 0) {
            for (int i = 0; i < gibsToSpawn; ++i) {
                int gib = Mathf.RoundToInt(Random.Range(0f, (float)gibTypes.Length-1));
                GameObject newGib = (GameObject)GameObject.Instantiate(gibTypes[gib], 
                    new Vector3(
                        this.transform.position.x + Random.Range(-spawnRange, spawnRange),
                        this.transform.position.y + Random.Range(-spawnRange, spawnRange),
                        this.transform.position.z + Random.Range(-spawnRange, spawnRange)),
                    Quaternion.Euler(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)));
                newGib.transform.parent = transform;
            }
        }

        if (gibSpawnOnce != null) {
            foreach (GameObject spawnOnce in gibSpawnOnce) {
                GameObject newGib = (GameObject)GameObject.Instantiate(spawnOnce,
                    new Vector3(
                        this.transform.position.x + Random.Range(-spawnRange, spawnRange),
                        this.transform.position.y + Random.Range(-spawnRange, spawnRange),
                        this.transform.position.z + Random.Range(-spawnRange, spawnRange)),
                    Quaternion.Euler(Random.Range(0f,360f),Random.Range(0f,360f),Random.Range(0f,360f)));
                newGib.transform.parent = transform;
            }
        }
    }//Start

    private void OnDrawGizmos() {
        Gizmos.color = Color.green; 
        Gizmos.DrawWireSphere(transform.position, spawnRange);
    }//OnDrawGizmos
}//Corpse
