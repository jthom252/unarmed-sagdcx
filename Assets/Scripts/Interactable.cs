﻿using UnityEngine;
using System.Collections;

public class Interactable : MonoBehaviour {
    public delegate void InteractAction();
    public delegate void InteractActionWithObject(GameObject other);
    public event InteractAction Interact;
    public event InteractActionWithObject InteractWithObject;
    
    private bool _activeState = false;
    public bool activeState {
        get {
            if (invertState) return !_activeState;
            return _activeState; 
        }
    }
    public bool invertState = false; 

    public void Execute(bool active=true)
    {
        if (Interact != null) {
            _activeState = active;
            Interact();
        }
    }

    public void Execute(GameObject other, bool active)
    {
        if (InteractWithObject != null) {
            _activeState = active; 
            InteractWithObject(other);
        }
    }
}//Interactable
