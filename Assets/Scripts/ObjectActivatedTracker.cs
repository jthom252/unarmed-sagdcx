﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class ObjectActivatedTracker : MonoBehaviour {
    public Interactable[] interactions;
    public Interactable[] interactTargets;
    private EventTrigger trigger; 

    public void Start() {
        if (this.GetComponent<EventTrigger>()) {
            trigger = this.GetComponent<EventTrigger>();
        } else {
            throw new MissingComponentException("A ObjectActivatedTracker is improperly configured (Has no EventTrigger)");
        }

        if (interactions.Length == 0) {
            Debug.LogWarning("An object activated trigger is empty and will automatically trigger"); 
            trigger.Trigger();
        } else {
            for (int i = 0; i < interactions.Length; ++i) {
                interactions[i].Interact += ObjectStatusChanged; 
            }
        }
    }//Start

    private void ObjectStatusChanged() {
        bool allObjectsActive = true; 
        for (int i = 0; i < interactions.Length; ++i) {
            if (!interactions[i].activeState) allObjectsActive = false; 
        }

        if (allObjectsActive) {
            trigger.Trigger();
            if (interactTargets != null && interactTargets.Length > 0) {
                foreach (Interactable interact in interactTargets) {
                    interact.Execute(); 
                }
            }
        }
    }//ObjectStatusChanged
}//ObjectActivatedTracker
