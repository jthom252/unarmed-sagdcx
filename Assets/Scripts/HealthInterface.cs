﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthInterface : MonoBehaviour {
    public GameObject HealthSpritePrefab;
    public Sprite HealthSpriteFull;
    public Sprite HealthSpriteEmpty;
    private float healthSpriteWidth = 25.0f;

    private Damageable playerDamageable;
    private ArrayList healthPoints;

    public void Init()
    {
        Debug.Log("HealthInterface init()");
        playerDamageable = GameHandler.player.GetComponent<Damageable>();
        if (playerDamageable == null)
        {
            throw new MissingComponentException("There is no Player object with the name 'Player'");
        }

        healthPoints = new ArrayList();
        Vector3 pointPos;
        for (int i = 0; i < playerDamageable.HitPoints; ++i)
        {
            GameObject point = Instantiate(HealthSpritePrefab, Vector3.zero ,Quaternion.identity) as GameObject;
            point.transform.parent = this.transform;

            RectTransform rectTransform = point.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector3((healthSpriteWidth + 5.0f) * i, 0.0f, 0.0f);

            healthPoints.Add(point);
        }
    }
	
	void Update () {
	    int playerHealth = playerDamageable.HitPoints;
	    GameObject obj;
	    for (int i = healthPoints.Count; i > 0; --i)
        {
            if (i > playerHealth)
            {
                obj = (GameObject) healthPoints[i - 1];
                obj.GetComponent<Image>().sprite = HealthSpriteEmpty;
            } else {
                obj = (GameObject) healthPoints[i - 1];
                obj.GetComponent<Image>().sprite = HealthSpriteFull;
            }
        }
	}
}
