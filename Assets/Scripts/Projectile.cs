﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
    public float moveSpeed;
    public bool randomBounce;
    public float timeToLive = 10f;
    public int maxBounces = 2; 

    private Rigidbody _rigidbody; 
    private float lifeTime; 
    private int bounces; 

    public Color fireElementColor = Color.red; 
    public Color iceElementColor  = Color.blue; 
    public Color normalElementColor = Color.white; 

    public Color playerFriendColor = new Color(0.6f, 0.6f, 1f); 
    public Color enemyFriendColor  = new Color(1f, 0.8f, 0.2f);
    public Color noFriendColor     = Color.gray; 

    public GameObject corpse; 
    public bool canChangeColor = true; 

    public AudioClip hitSound; 

	// Use this for initialization
	void Start () {
        if (GetComponent<Rigidbody>()) {
            _rigidbody = GetComponent<Rigidbody>(); 
        }

        if (GetComponent<Hazardous>()) { 
            GetComponent<Hazardous>().OnFriendChange += OnFriendChange;
            OnFriendChange();
        }

        if (GetComponent<Elemental>()) { 
            GetComponent<Elemental>().OnElementChange += OnElementChange; 
            OnElementChange(); 
        }

        lifeTime = 0f; 
        Explode(false);
	}//Start

	protected void FixedUpdate() {
        if (!GameHandler.Paused) {
            lifeTime += Time.deltaTime;
            if (lifeTime >= timeToLive && timeToLive != 0f) {
                Explode();
            }

            float rotation = this.transform.localEulerAngles.y * Mathf.PI / 180f;
            Vector3 nextPos = new Vector3(this.transform.position.x - (Mathf.Cos(rotation) * Time.deltaTime * moveSpeed),
                this.transform.position.y,
                this.transform.position.z - (Mathf.Sin(rotation) * Time.deltaTime * moveSpeed));
            Vector3 direction = this.transform.position - nextPos;

            RaycastHit[] hits = Physics.RaycastAll(this.transform.position, -direction.normalized, 0.5f, 1 << LayerMask.NameToLayer("Wall"));
            Debug.DrawRay(this.transform.position, -direction.normalized, Color.red);
            float rotateY = 180.0f;
            foreach (RaycastHit hit in hits) {
                Interactable obj = hit.collider.gameObject.GetComponent<Interactable>();
                if (obj != null) {
                    obj.Execute(gameObject, true);
                }

                if (randomBounce) {
                    rotateY = Random.Range(180.0f - 45.0f, 180.0f + 45.0f);
                }

                this.transform.Rotate(new Vector3(0.0f, rotateY, 0.0f));
                ++bounces;
                if (bounces >= maxBounces && maxBounces != 0) {
                    Explode();
                } else {
                    Explode(false);
                }
            }

            _rigidbody.MovePosition(nextPos);
        }
    }//Update

    public void Explode(bool destroy = true) {
        if (corpse != null) {
            GameObject newCorpse = (GameObject)GameObject.Instantiate(corpse, transform.position, Quaternion.identity);
            newCorpse.transform.parent = transform.parent;
        }
        GameHandler.PlayWorldAudio(hitSound);
        if (destroy) Destroy(gameObject);
    }//Explode

	private void OnCollisionEnter(Collision collision) {
        Explode();
    }//OnCollisionEnter

    private void OnFriendChange() {
        if (canChangeColor) {
            switch (GetComponent<Hazardous>().friend) {
                case Hazardous.Friend.Friend_Enemy: {
                        GetComponent<Renderer>().materials[0].color = enemyFriendColor;
                        break;
                    }

                case Hazardous.Friend.Friend_Player: {
                        GetComponent<Renderer>().materials[0].color = playerFriendColor;
                        break;
                    }

                case Hazardous.Friend.Friend_None: {
                        GetComponent<Renderer>().materials[0].color = noFriendColor;
                        break;
                    }

                default: {
                        break;
                    }
            }
        }
    }//OnFriendChange

    private void OnElementChange() {
        if (canChangeColor) {
            switch (GetComponent<Elemental>().element) {
                case Elemental.Element.Element_Fire: {
                        GetComponent<Renderer>().materials[1].color = fireElementColor;
                        break;
                    }

                case Elemental.Element.Element_Ice: {
                        GetComponent<Renderer>().materials[1].color = iceElementColor;
                        break;
                    }

                case Elemental.Element.Element_None: {
                        GetComponent<Renderer>().materials[1].color = normalElementColor;
                        break;
                    }

                default: {
                        break;
                    }
            }
        }
    }//OnElementChange
}//Projectile
