﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FloorSpikes : MonoBehaviour {
    public enum SpikeBehavior {
        PopOnTimer,
        PopOnInteract,
        PopOnWalkOver,
        PopOnOther
    }
    public SpikeBehavior behavior = SpikeBehavior.PopOnTimer; 
    public float deployTime = 3.5f; 
    public float activeTime = 1f; 
    public float popTime    = 0.2f; 
    public float waitTime   = 1f; 

    private Hazardous   hazard;
    private float       time; 
    private bool        isUp; 
    private bool        stayActive; 
    public GameObject   spikes; 
    public FloorSpikes  targetSpikes; 
    public bool         startActive; 
    private List<GameObject> objectsOnSpikes; 
    private bool        soundPlayed;

    public AudioClip popSound; 

    private void Start() {
        if (!GetComponent<Hazardous>()) {
            gameObject.AddComponent<Hazardous>(); 
        }
        hazard = GetComponent<Hazardous>();

        if (GetComponent<Interactable>()) {
            gameObject.AddComponent<Interactable>(); 
        }
        GetComponent<Interactable>().Interact += OnInteract; 

        time        = 0f;  
        stayActive  = GetComponent<Interactable>().activeState;
        isUp        = stayActive; 

        if (startActive) {
            time -= deployTime; 
            isUp  = true; 
        }

        objectsOnSpikes = new List<GameObject>(); 
        soundPlayed = false; 
    }//Start

    private void Update() {
        time += Time.deltaTime;

        if (!stayActive) {
            if (isUp) {
                if (time >= activeTime) {
                    isUp = false;
                    time = 0f;
                    if (targetSpikes != null) { 
                        targetSpikes.Activate(); 
                    }
                }
            } else if (behavior == SpikeBehavior.PopOnTimer) {
                if (time >= deployTime) {
                    isUp = true;
                    time = 0f;
                    soundPlayed = false; 
                }
            }
        }

        if (isUp) {
            PopUp(time/popTime);
        } else {
            Retract(time/popTime); 
        }
    }//Update

    public void Activate() {
        Debug.Log("Activate");
        isUp = true; 
        time = 0f; 
    }//Activate

    private void OnTriggerEnter(Collider col) {
        if (behavior == SpikeBehavior.PopOnWalkOver && !isUp) {
            time = 0f - waitTime; 
            isUp = true; 
        }

        if (col.GetComponent<Damageable>()) objectsOnSpikes.Add(col.gameObject);
    }//OnTriggerEnter

    private void OnTriggerExit(Collider col) {
        if (col.GetComponent<Damageable>()) objectsOnSpikes.Remove(col.gameObject);
    }//OnTriggerExit

    private void DealDamage() {
        for (int i = 0; i < objectsOnSpikes.Count; ++i) {
            if (objectsOnSpikes[i] != null) objectsOnSpikes[i].GetComponent<Damageable>().DealDamage(hazard);
            objectsOnSpikes.Remove(objectsOnSpikes[i]);
        }
    }//DealDamage

    private void PopUp(float progress) {
        spikes.transform.localPosition = new Vector3(
            spikes.transform.localPosition.x,
            Mathf.Lerp(-1f, 0f, progress),
            spikes.transform.localPosition.z);
        if (progress > 0.9f) {
            DealDamage();
            if (!soundPlayed) {
                GameHandler.PlayWorldAudio(popSound);
                soundPlayed = true; 
            }
        }
    }//PopUp

    private void Retract(float progress) {
        spikes.transform.localPosition = new Vector3(
            spikes.transform.localPosition.x,
            Mathf.Lerp(0f, -1f, progress),
            spikes.transform.localPosition.z);
        if (progress > 0.9f) {
            soundPlayed = false; 
        }
    }//Retract

    private void OnInteract() {
        if (behavior == SpikeBehavior.PopOnInteract) {
            if (GetComponent<Interactable>().activeState) {
                time = 0f; 
                isUp = true; 
                stayActive = true;
            } else {
                time = 0f; 
                isUp = false;  
                stayActive = false;
                soundPlayed = false;
            }
        }
    }//OnInteract

    private void OnDrawGizmos() {
        if (targetSpikes != null) {
            Gizmos.color = Color.red; 
            Gizmos.DrawSphere(transform.position, 0.1f);
            Gizmos.DrawLine(transform.position, targetSpikes.transform.position);
        }
    }//OnDrawGizmos
}//FloorSpikes
