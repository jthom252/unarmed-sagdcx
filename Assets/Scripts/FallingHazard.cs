﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class FallingHazard : MonoBehaviour {
    private void OnTriggerEnter(Collider col) {
        if (col.GetComponent<Damageable>()) {
            col.transform.localPosition = col.GetComponentInParent<Room>().respawnPoint; 
            col.GetComponent<Damageable>().DealDamage(1000);
        }
    }//OnTriggerEnter

    private void OnDrawGizmos() {
        BoxCollider box = GetComponent<BoxCollider>(); 
        Gizmos.color = new Color(1f,0f,1f,0.25f); 
        Gizmos.DrawCube(box.transform.position, box.size); 
    }//OnDrawGizmos
}//FallingHazard
