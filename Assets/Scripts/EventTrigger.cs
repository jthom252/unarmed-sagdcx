﻿using UnityEngine;
using System.Collections;

public class EventTrigger : MonoBehaviour {
    public event GameHandler.GameEvent OnTriggered;

    public void Trigger() {
        if (OnTriggered != null) OnTriggered(); 
    }//Trigger
}//EventTrigger
