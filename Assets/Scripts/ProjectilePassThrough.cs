﻿using UnityEngine;
using System.Collections;

public class ProjectilePassThrough : MonoBehaviour {
    public GameHandler.GameEvent ProjectileEntered; 
    public GameHandler.GameEventWithObject ProjectileEnteredWithObject;

	void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.GetComponent<Projectile>() != null) {
            if (ProjectileEntered != null) {
                ProjectileEntered(); 
            }

            if (ProjectileEnteredWithObject != null)
            {
                ProjectileEnteredWithObject(collider.gameObject);
            }
        }
    }//OnTriggerEnter
}//ProjectilePassThrough
