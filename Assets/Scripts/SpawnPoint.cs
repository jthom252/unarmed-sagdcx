﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {
    public enum SpawnBehavior {
        OnRoomEnter,
        OnSpawnTrigger,
        OnRoomComplete
    }

    public GameObject spawnObject;
    public float RotationX;
    public float RotationY;
    public float RotationZ;
    public SpawnBehavior behavior = SpawnBehavior.OnRoomEnter; 

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.6f,1f,0.8f,0.5f);
        Gizmos.DrawIcon(transform.position, "SpawnPointMarker.png", true);

        if (spawnObject != null) {
            Transform[] t = spawnObject.GetComponentsInChildren<Transform>(true);

            foreach (Transform tr in t) {
                if (tr.GetComponent<MeshFilter>()) {
                    if (tr.GetComponent<MeshFilter>().sharedMesh != null) {
                        for (int i = 0; i < tr.GetComponent<MeshFilter>().sharedMesh.subMeshCount; ++i) {
                            Gizmos.DrawMesh(tr.GetComponent<MeshFilter>().sharedMesh, i,
                                transform.position,
                                Quaternion.Euler(RotationX, RotationY, RotationZ));
                        }
                    }
                }

                MeshFilter[] childFilters = tr.GetComponentsInChildren<MeshFilter>();
                if (childFilters != null) {
                    for (int i = 0; i < childFilters.Length; ++i) {
                        for (int j = 0; j < childFilters[i].sharedMesh.subMeshCount; ++j) {
                            Gizmos.DrawMesh(childFilters[i].sharedMesh, j, transform.position, Quaternion.Euler(RotationX, RotationY, RotationZ));
                        }
                    }
                }
            }
        } 
    }//OnDrawGizmos

    public void Spawn() {
        if (spawnObject != null) {
            GameObject spawned = (GameObject)Instantiate(spawnObject,
                                                        transform.position,
                                                        Quaternion.Euler(RotationX, RotationY, RotationZ));
            spawned.transform.parent = this.transform.parent;
        }
    }//SpawnObject
}//SpawnPoint
