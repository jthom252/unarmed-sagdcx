﻿using UnityEngine;
using System.Collections;

public class FloaterEnemy : MonoBehaviour {
    public GameObject    projectile;
    public float         fireDistance = 2f;
    public float         fireDelay    = 2f;
    public AudioClip     fireSound; 
    public AudioClip     deathSound; 
    public GameObject    corpse; 

    private NavMeshAgent navAgent; 
    private float        fireTime; 

    private Hazardous.Friend friend;

	void Start () {
        if (this.GetComponent<Damageable>() == null) {
            this.gameObject.AddComponent<Damageable>();
        }
        this.GetComponent<Damageable>().OnDeath += OnDeath;

        if (this.GetComponent<NavMeshAgent>() == null) {
            throw new MissingComponentException("Enemy has no NavMesh Agent"); 
        }
        navAgent = GetComponent<NavMeshAgent>();

        Hazardous hazard = this.GetComponent<Hazardous>();
        if (hazard != null)
        {
            friend = hazard.friend;
        } else {
            friend = Hazardous.Friend.Friend_Enemy;
        }
 
        fireTime = 0f; 
	}//Start
	
	void Update () {
	    navAgent.destination = GameHandler.player.transform.position; 

        float dist = Vector3.Distance(this.transform.position, GameHandler.player.transform.position);
        if (dist <= fireDistance) {
            navAgent.destination = this.transform.position; 
            transform.LookAt(GameHandler.player.transform);

            fireTime += Time.deltaTime;
            if (fireTime >= fireDelay) {
                SpawnProjectile(GetAngleToPlayer()); 
                fireTime = 0f; 
            }
        } else {
            fireTime = 0f;
        }
	}//Update

    float GetAngleToPlayer() {
        float xAngle = (this.transform.position.x + (this.transform.forward.x / 2)) - GameHandler.player.transform.position.x;
        float zAngle = (this.transform.position.z + (this.transform.forward.z / 2)) - GameHandler.player.transform.position.z;
        return Mathf.Atan2(zAngle, xAngle) * 180f / Mathf.PI;
    }//GetAngleToPlayer

    void SpawnProjectile(float projectileRotation) {
        GameObject projectileObject = GameObject.Instantiate(projectile,
                                                       this.transform.position + (this.transform.forward / 2),
                                                       Quaternion.Euler(new Vector3(0f, projectileRotation))) as GameObject;
        projectileObject.transform.parent = this.transform.parent; 
        GameHandler.PlayWorldAudio(fireSound);

        Hazardous projectileHazard = projectileObject.gameObject.GetComponent<Hazardous>();
        if (projectileHazard != null)
        {
            projectileHazard.friend = friend;
        }
    }//SpawnProjectile

    private void OnDeath() {
        Damageable damageable = this.GetComponent<Damageable>();
        if (damageable != null) {
            if (!damageable.IsAlive()) {
                GameHandler.PlayWorldAudio(deathSound);
                if (corpse != null) {
                    GameObject newCorpse = (GameObject)GameObject.Instantiate(corpse, transform.position, Quaternion.identity);
                    newCorpse.transform.parent = transform.parent;
                }
                Destroy(this.gameObject);
            }
        }
    }//HitByProjectile
}//FloaterEnemy
