﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;
using System.Collections.Generic;

public class Fader : MonoBehaviour {
    private static List<Fader> faders; 
    private Image image;
    private bool isFadingOut; 
    private float fadeTime;
    private bool called; 
    private GameHandler.GameEvent call; 
    public float fadeTimeTotal = 0.8f; 

	private void Start () {
	    if (faders == null) faders = new List<Fader>(); 
        faders.Add(this);
        image = GetComponent<Image>(); 
        called = false; 
	}//Start

    private void Update() {
        if (fadeTime <= fadeTimeTotal) {
            fadeTime += Time.deltaTime; 
            if (isFadingOut) {
                image.material.color = new Color(0f,0f,0f,fadeTime/fadeTimeTotal); 
            } else {
                image.material.color = new Color(0f, 0f, 0f,1f-fadeTime/fadeTimeTotal); 
            }
        } else if (!called) {
            if (call != null) call(); 
            called = true; 
        }
    }//Update

    public static void FadeOut(GameHandler.GameEvent callback = null) {
        if (faders != null) {
            foreach (Fader fade in faders) {
                fade.fadeTime = 0;
                fade.isFadingOut = true;
                fade.called = false;
                fade.call = callback;
            }
        }
    }//FadeOut

    public static void FadeIn(GameHandler.GameEvent callback = null) {
        if (faders != null) {
            foreach (Fader fade in faders) {
                fade.fadeTime = 0;
                fade.isFadingOut = false;
                fade.called = false;
                fade.call = callback;
            }
        }
    }//FadeIn
}//Fader
