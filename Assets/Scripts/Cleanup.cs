﻿using UnityEngine;
using System.Collections;

public class Cleanup : MonoBehaviour {
    //This is a simple component used to cleanup objects 
    //that need to be removed when the player exits a 
    //room. 
    public delegate void CleanupEvent(); 
    public event CleanupEvent OnCleanup;

    public bool autoCleanup = false; 
    public float autoCleanupTime = 5f; 
    private float cleanupTime; 

    public void Process() {
        if (OnCleanup != null) {
            OnCleanup();
        } else {
            Destroy(this.gameObject); 
        }
    }//Process

    private void Start() {
        cleanupTime = 0; 
    }//Start

    private void Update() {
        if (autoCleanup) {
            cleanupTime += Time.deltaTime;
            if (cleanupTime >= autoCleanupTime) {
                Process(); 
            }
        }
    }//Update
}//Cleanup
