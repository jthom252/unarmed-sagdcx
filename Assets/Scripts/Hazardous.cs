﻿using UnityEngine;
using System.Collections;

public class Hazardous : MonoBehaviour {
    public GameHandler.GameEvent OnFriendChange; 

    public enum Friend
    {
        Friend_Player,
        Friend_Enemy,
        Friend_None
    };

    public int Damage;

    private Friend _friend = Friend.Friend_Enemy; 
    public Friend friend {
        get {
            return _friend; 
        }

        set {
            _friend = value; 
            if (OnFriendChange != null) OnFriendChange(); 
        }
    }
}//Hazardous
