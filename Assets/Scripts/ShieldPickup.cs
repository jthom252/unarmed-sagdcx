﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;

public class ShieldPickup : MonoBehaviour {
    public float gravitateDist = 5f; 
    public float gravitateSpeed = 2f; 
    public Interactable interactTarget; 
    public GameObject replacementObject; 

    public Text pickupTextObject; 

    public AudioClip pickupSound; 

    void FixedUpdate() {
        if (Vector3.Distance(this.transform.position, GameHandler.player.transform.position) <= gravitateDist) {
            transform.position = Vector3.MoveTowards(this.transform.position, 
                GameHandler.player.transform.position+new Vector3(0f,1f), 
                gravitateSpeed * Time.deltaTime);
        }

        if (replacementObject != null) {
            SetShield(replacementObject);
        } else {
            Debug.Log("Blank Shield Pickup");
            Destroy(gameObject); 
        }
    }//Update

    void GiveShield() {
        GameHandler.player.GiveShield(replacementObject);
        if (interactTarget) interactTarget.Execute();
        GameHandler.PlayWorldAudio(pickupSound);
    }//GiveShield

    void OnTriggerEnter(Collider col) {
        if (col.gameObject.layer == Top.LAYER_PLAYER && col.GetComponent<Player>()) {
            GiveShield();
            Destroy(this.gameObject);
        }
    }//OnTriggerEnter

    public void SetShield(GameObject newShield) {
        replacementObject = newShield.gameObject; 
        this.GetComponent<MeshFilter>().sharedMesh = newShield.GetComponent<MeshFilter>().sharedMesh; 
        this.GetComponent<MeshRenderer>().sharedMaterials = newShield.GetComponent<MeshRenderer>().sharedMaterials;
        newShield.SetActive(false);
    }//SetShield

    void OnDrawGizmos() {
        if (interactTarget) {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(this.transform.position, interactTarget.transform.position);
        }
    }//OnDrawGizmos
}//ShieldPickup
