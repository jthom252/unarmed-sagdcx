﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PressurePlate : MonoBehaviour {
    public bool Switch = false;
    public List<Interactable> TriggerInteractables;
    private List<GameObject> occupiedObjects;
    private bool switchTriggered = false;
    public AudioClip clickSound;

    void Start()
    {
        occupiedObjects = new List<GameObject>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == Top.LAYER_PLAYER) {
            GameHandler.PlayWorldAudio(clickSound);
            if (!Switch || !switchTriggered) {
                ExecuteInteractables();
                occupiedObjects.Add(other.gameObject);
                switchTriggered = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == Top.LAYER_PLAYER) {
            if (!Switch) {
                occupiedObjects.Remove(other.gameObject);
                if ((occupiedObjects.Count == 0))
                    ExecuteInteractables(false);
            }
        }
    }

    private void ExecuteInteractables(bool active = true) {
        foreach (Interactable interactable in TriggerInteractables) {
            if (interactable != null) interactable.Execute(active); 
        }
    }//ExecuteInteractables

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow; 
        foreach (Interactable interactable in TriggerInteractables) {
            if (interactable != null) Gizmos.DrawLine(this.transform.position, interactable.transform.position);
        }
    }//OnDrawGizmos
}//PressurePlate
