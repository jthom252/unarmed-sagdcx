﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class InputHandler : MonoBehaviour {
    //Maybe change this to a singleton later? Needs to have a game object
    //to receive Update() and input events however. 

    private const float TOUCH_MIN_DIST = 64f; 
    public delegate void InputEvent(bool pressed = true);

    public enum InputType {
        Keyboard,
        Mouse,
        Joystick
    }

    public static event InputEvent MoveLeft; 
    public static event InputEvent MoveRight;
    public static event InputEvent MoveUp; 
    public static event InputEvent MoveDown;
    public static event InputEvent Aim; 
    public static event InputEvent Respawn;
    public static event InputEvent Mute; 
    public static event InputEvent Skip;
    public static event InputEvent Talk; 
    public static event InputEvent Pause; 
    public static event InputEvent Cheat; 

    public enum TargetEvent {
        MoveLeft,
        MoveRight,
        MoveUp,
        MoveDown,
        Aim,
        Respawn,
        Mute,
        Skip,
        Talk,
        Pause,
        Cheat
    }

    public enum InputMode {
        Keyboard,
        Joystick,
        Touch
    }

    public struct KeyBind {
        public InputType   type; 
        public KeyCode     key; 
        public TargetEvent action;
        public int         button; 
        public bool        active; 

        public KeyBind(InputType t, TargetEvent a, KeyCode k = 0, int btn = 0) {
            type     = t; 
            key      = k;
            action   = a; 
            button   = btn;
            active   = false; 
        }

        public void FireEvent(bool p = true) {
            switch (action) {
                case TargetEvent.MoveUp: if (MoveUp != null) MoveUp(p); break;
                case TargetEvent.MoveDown: if (MoveDown != null) MoveDown(p); break;
                case TargetEvent.MoveLeft: if (MoveLeft != null) MoveLeft(p); break;
                case TargetEvent.MoveRight: if (MoveRight != null) MoveRight(p); break;
                case TargetEvent.Aim: if (Aim != null) Aim(p); break;
                case TargetEvent.Respawn: if (Respawn != null) Respawn(p); break;
                case TargetEvent.Mute: if (Mute != null) Mute(p); break;
                case TargetEvent.Skip: if (Skip != null) Skip(p); break;
                case TargetEvent.Talk: if (Talk != null) Talk(p); break;
                case TargetEvent.Pause: if (Pause != null) Pause(p); break; 
                case TargetEvent.Cheat: if (Cheat != null) Cheat(p); break;
            }
        }
    }

    public struct TouchStick {
        public Vector2 startPosition; 
        public int fingerId; 
        public float xDiff; 
        public float yDiff;

        public TouchStick(int id = 0) {
            startPosition = new Vector2(0f,0f); 
            fingerId = id; 
            xDiff = 0f; 
            yDiff = 0f; 
        }

        public void Reset() {
            startPosition = new Vector2(0f,0f);
            xDiff = 0f; 
            yDiff = 0f; 
        }
    }
    private static TouchStick leftTouchStick; 
    private static TouchStick rightTouchStick; 

    private static KeyBind[]     keybinds; 
    private static InputMode _inputMode = InputMode.Keyboard;
    public static InputMode inputMode {
        get {
            return _inputMode; 
        }
    }

    private static float _controllerAngle;
    public static float controllerAngle {
        get {
            return _controllerAngle; 
        }
    }
    private static float analogX; 
    private static float analogY;

    private bool keyLeft;
    private bool keyRight;
    private bool keyUp;
    private bool keyDown; 

    public static float GetXMovement() {
        if (analogX == 0f) return 0f;
        return Mathf.Sin(controllerAngle * Mathf.Deg2Rad);
    }//GetXMovement

    public static float GetZMovement() {
        if (analogY == 0f) return 0f;
        return Mathf.Cos(controllerAngle * Mathf.Deg2Rad);
    }//GetZMovement

    private static void SetControllerAngle(float x = 0f, float y = 0f) {
        analogX = x; 
        analogY = y; 

        _controllerAngle = Mathf.Atan2(x, y) * 180 / Mathf.PI;
    }//GetControllerAngle

    public static float GetMouseRotation() {
        if (_inputMode == InputMode.Keyboard) {
            float mouseRotX = (Screen.width / 2) - Input.mousePosition.x;
            float mouseRotY = (Screen.height / 2) - Input.mousePosition.y;
            return Mathf.Atan2(mouseRotX, -mouseRotY) * 180 / Mathf.PI;
        }

        if (_inputMode == InputMode.Touch) {
            float touchRotX = rightTouchStick.xDiff; 
            float touchRotY = rightTouchStick.yDiff;
            return Mathf.Atan2(-touchRotX, touchRotY) * 180 / Mathf.PI;
        }

        return Mathf.Atan2(-Input.GetAxis("JoyRX"), -Input.GetAxis("JoyRY")) * 180 / Mathf.PI;  
    }//GetMouseRotation

    public void Start() {
        keybinds = new KeyBind[]{
        new KeyBind(InputType.Keyboard, TargetEvent.MoveUp, KeyCode.W),
        new KeyBind(InputType.Keyboard, TargetEvent.MoveUp, KeyCode.UpArrow),
        new KeyBind(InputType.Keyboard, TargetEvent.MoveLeft, KeyCode.A),
        new KeyBind(InputType.Keyboard, TargetEvent.MoveLeft, KeyCode.LeftArrow),
        new KeyBind(InputType.Keyboard, TargetEvent.MoveRight, KeyCode.D),
        new KeyBind(InputType.Keyboard, TargetEvent.MoveRight, KeyCode.RightArrow),
        new KeyBind(InputType.Keyboard, TargetEvent.MoveDown, KeyCode.S),
        new KeyBind(InputType.Keyboard, TargetEvent.MoveDown, KeyCode.DownArrow),
        new KeyBind(InputType.Keyboard, TargetEvent.Respawn, KeyCode.R),
        new KeyBind(InputType.Joystick, TargetEvent.Respawn, KeyCode.JoystickButton0),
        new KeyBind(InputType.Keyboard, TargetEvent.Skip, KeyCode.P),
        new KeyBind(InputType.Keyboard, TargetEvent.Cheat, KeyCode.I),
        new KeyBind(InputType.Keyboard, TargetEvent.Mute, KeyCode.M),
        new KeyBind(InputType.Joystick, TargetEvent.Mute, KeyCode.JoystickButton6),
        new KeyBind(InputType.Keyboard, TargetEvent.Pause, KeyCode.Escape),
        new KeyBind(InputType.Joystick, TargetEvent.Pause, KeyCode.JoystickButton7),
        new KeyBind(InputType.Keyboard, TargetEvent.Talk, KeyCode.E),
        new KeyBind(InputType.Joystick, TargetEvent.Talk, KeyCode.JoystickButton2),
        new KeyBind(InputType.Mouse, TargetEvent.Aim, 0, 0), 
        new KeyBind(InputType.Keyboard, TargetEvent.Aim, KeyCode.LeftControl)};

        analogX = 0f;
        analogY = 0f; 

        MoveLeft += KeyboardLeft; 
        MoveRight += KeyboardRight; 
        MoveUp += KeyboardUp;
        MoveDown += KeyboardDown;  

        leftTouchStick = new TouchStick(); 
        rightTouchStick = new TouchStick(); 
    }//Start

    private void KeyboardLeft(bool pressed = true) {
        keyLeft = pressed; 
    }//KeyboardLeft

    private void KeyboardRight(bool pressed = true) {
        keyRight = pressed;
    }//KeyboardRight

    private void KeyboardUp(bool pressed = true) {
        keyUp = pressed; 
    }//KeyboardUp

    private void KeyboardDown(bool pressed = true) {
        keyDown = pressed;
    }//KeyboardDown

    private void KeyboardToAnalog() {
        int x = 0; 
        int y = 0; 

        if (keyLeft) --x; 
        if (keyRight) ++x; 
        if (keyUp) ++y; 
        if (keyDown) --y; 
        SetControllerAngle((float)x,(float)y); 
    }//KeyboardToAnalog

    public void OnGUI() {
        Event e = Event.current;
        if (e.isKey && e.type == EventType.KeyDown) {
            string str = e.keyCode.ToString();
            if (str.Length == 1) {
                CheatSystem.AddToCheat(str); 
            }
        }
    }//OnGUI

	public void Update () {
        for (int t = 0; t < Input.touchCount; ++t) {
            _inputMode = InputMode.Touch; 

            //Touch started
            if (Input.GetTouch(t).phase == TouchPhase.Began) {
                if (Input.GetTouch(t).position.x < (Screen.width / 2) && Input.GetTouch(t).position.y < (Screen.height / 2)) {
                    leftTouchStick.fingerId = Input.GetTouch(t).fingerId; 
                    leftTouchStick.startPosition = Input.GetTouch(t).position;
                } else if (Input.GetTouch(t).position.x > (Screen.width / 2) && Input.GetTouch(t).position.y < (Screen.height / 2)) {
                    Aim(true); 
                    rightTouchStick.fingerId = Input.GetTouch(t).fingerId;
                    rightTouchStick.startPosition = Input.GetTouch(t).position;
                } else if (Input.GetTouch(t).tapCount == 2) {
                    Talk(true);
                    Respawn(true); 
                }
            }

            //Touch moved
            if (Input.GetTouch(t).phase == TouchPhase.Moved) {
                if (Input.GetTouch(t).fingerId == leftTouchStick.fingerId) {
					leftTouchStick.xDiff = Input.GetTouch(t).position.x - leftTouchStick.startPosition.x;
					leftTouchStick.yDiff = Input.GetTouch(t).position.y - leftTouchStick.startPosition.y;
                } else if (Input.GetTouch(t).fingerId == rightTouchStick.fingerId) {
					rightTouchStick.xDiff = Input.GetTouch(t).position.x - rightTouchStick.startPosition.x;
					rightTouchStick.yDiff = Input.GetTouch(t).position.y - rightTouchStick.startPosition.y;
                }
            }

            //Touch ended
            if (Input.GetTouch(t).phase == TouchPhase.Ended) {
                if (Input.GetTouch(t).fingerId == leftTouchStick.fingerId) {
                    SetControllerAngle();
                    leftTouchStick.Reset();
                } else if (Input.GetTouch(t).fingerId == rightTouchStick.fingerId) {
                    Aim(false); 
                    rightTouchStick.Reset(); 
                }
            }
        }

        //Use touch joysticks
        if (leftTouchStick.xDiff > TOUCH_MIN_DIST || leftTouchStick.xDiff < -TOUCH_MIN_DIST) {
            SetControllerAngle(leftTouchStick.xDiff, analogY);
        } else if (_inputMode == InputMode.Touch) {
            SetControllerAngle(0f, analogY);
        }

        if (leftTouchStick.yDiff > TOUCH_MIN_DIST || leftTouchStick.yDiff < -TOUCH_MIN_DIST) {
            SetControllerAngle(analogX, leftTouchStick.yDiff);
        } else if (_inputMode == InputMode.Touch) {
            SetControllerAngle(analogX, 0f);
        }

        int i; 
        if (keybinds != null) {
            //Iterate and send actions for each keybind
            for (i = 0; i < keybinds.Length; ++i) {
                switch (keybinds[i].type) {
                    case InputType.Keyboard: {
                        if (Input.GetKey(keybinds[i].key) && !keybinds[i].active) {
                            keybinds[i].FireEvent(true);
                            keybinds[i].active = true; 
                            _inputMode = InputMode.Keyboard; 
                        } else if (!Input.GetKey(keybinds[i].key) && keybinds[i].active) {
                            keybinds[i].FireEvent(false);
                            keybinds[i].active = false; 
                        }
                        break; 
                    }

                    case InputType.Mouse: {
						if (_inputMode == InputMode.Keyboard) {
	                        if (Input.GetMouseButton(keybinds[i].button) && !keybinds[i].active) {
	                            keybinds[i].FireEvent(true);
	                            keybinds[i].active = true; 
	                        } else if (!Input.GetMouseButton(keybinds[i].button)) {
	                            keybinds[i].FireEvent(false);
	                            keybinds[i].active = false; 
	                        }
						}
                        break;
                    }

                    case InputType.Joystick: {
                        if (Input.GetKey(keybinds[i].key) && !keybinds[i].active) {
                            keybinds[i].FireEvent(true);
                            keybinds[i].active = true;
                            _inputMode = InputMode.Joystick;
                        } else if (!Input.GetKey(keybinds[i].key) && keybinds[i].active) {
                            keybinds[i].FireEvent(false);
                            keybinds[i].active = false;
                        }
                        break; 
                    }
                }
            }
        }//End keybinds

        if (_inputMode == InputMode.Keyboard) KeyboardToAnalog(); 

        //Real rough controller support
        bool xMoved = false; 
        bool yMoved = false; 
        if (Input.GetAxis("JoyLX") > 0.33f || Input.GetAxis("JoyLX") < -0.33f) {
            _inputMode = InputMode.Joystick; 
            SetControllerAngle(Input.GetAxis("JoyLX"), analogY); 
            xMoved = true; 
        }

        if (Input.GetAxis("JoyLY") > 0.33f || Input.GetAxis("JoyLY") < -0.33f) {
            _inputMode = InputMode.Joystick;
            SetControllerAngle(analogX, -Input.GetAxis("JoyLY")); 
            yMoved = true; 
        }

        if (inputMode == InputMode.Joystick) {
            if (Input.GetAxis("JoyRX") > 0.2f || Input.GetAxis("JoyRX") < -0.2f || Input.GetAxis("JoyRY") > 0.2f || Input.GetAxis("JoyRY") < -0.2f) {
                Aim(true);
            } else {
                Aim(false);
            }
        }

        if (inputMode == InputMode.Joystick && !xMoved && !yMoved) {
            SetControllerAngle(); 
        } 
	}//Update
}//InputHandler
