﻿using UnityEngine;
using System.Collections;

public class Wizard : MonoBehaviour {
    public Hazardous.Friend friend = Hazardous.Friend.Friend_Enemy; 
    private Animator _animator;

    private Damageable damageable;
    private float iterationTime;
    private bool activeMode; 
    private bool projectileSpawned; 
    private int lastWarp; 
    private int iteration; 
    private float warpTime; 
    private Vector3 startPos; 

    private enum WizardBehavior {
        HomingSkulls,
        Warp,
        MurderBeam,
        Wait
    }
    private WizardBehavior lastBehavior; 
    private WizardBehavior currentBehavior; 
    public float HomingSkullBehaviorDuration = 1f; 
    public float HomingSkullSpawnInterval  = 2f; 
    public float WarpBehaviorDuration = 2f; 
    public float WarpInterval        = 2f; 
    public float WarpSpinTime        = 1f; 
    public float MurderBehaviorDuration = 2f; 
    public float MurderFireRate     = 0.2f; 
    public float WaitBehaviorDuration = 0.5f; 
    public float StartingDistance       = 10f; 

    public GameObject corpse;
    public GameObject homingSkull;
    public GameObject murderBeam;
    public GameObject normalProjectile;
    public GameObject warpEffect; 
    public GameObject skullMesh; 
    public Vector3[] warpPoints; 

    public AudioClip hitSound;
    public AudioClip deathSound; 
    public AudioClip aggroSound;

    // Use this for initialization
    public void Start() {
        if (this.GetComponent<Animator>() == null) {
            throw new MissingComponentException("No animator attached to Wizard.");
        }
        _animator = this.GetComponent<Animator>();

        if (this.GetComponent<Damageable>() == null) {
            throw new MissingComponentException("No damage component on wizard");
        }
        damageable = this.GetComponent<Damageable>(); 
        damageable.OnHit += OnHit; 
        damageable.OnDeath += OnDeath; 

        iterationTime = 0f; 
        activeMode = false; 
        iteration = 0;
        projectileSpawned = false; 
        lastBehavior = WizardBehavior.Wait; 
        currentBehavior = WizardBehavior.Wait;
        GetComponentInChildren<SkinnedMeshRenderer>().enabled = false; 
        startPos = this.transform.position; 

        lastWarp = 0; 
        warpTime = 0f; 
    }//Start

    public void Update() {
        if (activeMode) {
            if (warpTime >= 0) {
                _animator.SetBool("HandsUp", true);
                warpTime -= Time.deltaTime;
                skullMesh.transform.Rotate(new Vector3(0f,Time.deltaTime * 600f)); 
                if (warpTime <= 0) {
                    RandomWarp();
                    _animator.SetBool("HandsUp", false);
                }
            } else {
                skullMesh.transform.LookAt(GameHandler.player.transform);
                iterationTime += Time.deltaTime;
            }

            switch (currentBehavior) {
                case WizardBehavior.Wait: {
                        PickNewBehavior();
                        break;
               }

                case WizardBehavior.HomingSkulls: {
                        FireHomingSkulls();
                        break;
                }

                case WizardBehavior.MurderBeam: {
                        MurderBeam();
                        break;
                }

                case WizardBehavior.Warp: {
                        Warp(); 
                        break;
                }
            }
        } else {
            if (Vector3.Distance(transform.position, GameHandler.player.transform.position) <= StartingDistance) {
                activeMode = true; 
                warpTime = WarpSpinTime; 
                SpawnProjectileCluster();
                GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
                GetComponentInChildren<ShieldCluster>().Activate(); 
                GameHandler.PlayWorldAudio(aggroSound);
            }
        }
    }//Update

    //--------------------------------
    //Behaviors
    //--------------------------------
    private void PickNewBehavior() {
        if (iterationTime >= WaitBehaviorDuration) {

            int select = Random.Range(0, 3);
            switch (select) {
                case 0: {
                        currentBehavior = WizardBehavior.HomingSkulls;
                        break;
                    }

                case 1: {
                        currentBehavior = WizardBehavior.MurderBeam;
                        break;
                    }

                case 2: {
                        currentBehavior = WizardBehavior.Warp;
                        break;
                    }
            }

            //Default to murder beam
            if (currentBehavior == lastBehavior) currentBehavior = WizardBehavior.MurderBeam; 
            iterationTime = 0f; 
        }
    }//PickNewBehavior

    //--------------------------------
    //Homing Skulls
    //--------------------------------
    private void SpawnSkull(Vector3 offset) {
        if (homingSkull != null) {
            GameObject skull = (GameObject)GameObject.Instantiate(
                homingSkull,
                transform.position + offset,
                Quaternion.identity);
            skull.transform.parent = transform.parent; 
        }
    }//SpawnSkull

    private void FireHomingSkulls() {
        _animator.SetBool("Block", true);

        if (iterationTime >= iteration * HomingSkullSpawnInterval) {
            ++iteration;
            SpawnSkull(new Vector3(1f, 0f));
            SpawnSkull(new Vector3(-1f, 0f));
        }

        if (iterationTime >= HomingSkullBehaviorDuration) {
            _animator.SetBool("Block", false);
            currentBehavior = WizardBehavior.Wait; 
            lastBehavior = WizardBehavior.HomingSkulls;
            iteration = 0; 
            iterationTime = 0;
            warpTime = WarpSpinTime;  
        }
    }//FireHomingSkulls

    //--------------------------------
    //Murder Beams
    //--------------------------------
    private void SpawnMurderBeam() {
        float xAngle = skullMesh.transform.position.x - GameHandler.player.transform.position.x;
        float zAngle = skullMesh.transform.position.z - GameHandler.player.transform.position.z;
        float rotation = Mathf.Atan2(zAngle, xAngle) * 180f / Mathf.PI;

        GameObject murder = (GameObject)GameObject.Instantiate(
            murderBeam, 
            skullMesh.transform.position + new Vector3(0f,1f), 
            Quaternion.Euler(new Vector3(0f,rotation,0f)));
        murder.transform.parent = transform.parent; 
    }//SpawnMurderBeam

    private void MurderBeam() {
        _animator.SetBool("Block", true);

        if (iterationTime >= MurderFireRate * iteration) {
            SpawnMurderBeam();
            iteration++;
        }

        if (iterationTime >= MurderBehaviorDuration) {
            _animator.SetBool("Block", false);
            projectileSpawned = false;
            currentBehavior = WizardBehavior.Wait;
            lastBehavior = WizardBehavior.MurderBeam;
            iterationTime = 0;
            warpTime = WarpSpinTime;
        }
    }//MurderBeam

    //--------------------------------
    //Projectile Spawning
    //--------------------------------
    private void SpawnProjectileCluster() {
        int angle = 0;
        for (int i = 0; i < 8; ++i) {
            SpawnProjectile(angle + (i * 45));
        }
    }//SpawnProjectileCluster

    private void SpawnProjectile(float projectileRot) {
        Vector3 adjustPos = new Vector3(
            1.8f * -Mathf.Cos(projectileRot * Mathf.Deg2Rad),
            1f,
            1.8f * -Mathf.Sin(projectileRot * Mathf.Deg2Rad));

        GameObject projectile = (GameObject)GameObject.Instantiate(
            normalProjectile,
            this.transform.position + adjustPos,
            Quaternion.Euler(new Vector3(0f,projectileRot,0f)));
        projectile.transform.parent = transform.parent; 
    }//SpawnProjectile

    //--------------------------------
    //Warping
    //--------------------------------
    private void RandomWarp() {
        SpawnWarpEffect();

        int warp = Random.Range(0, warpPoints.Length - 1);
        if (warp == lastWarp) {
            ++warp; 
            if (warp > warpPoints.Length) warp = 0; 
        }

        if (Vector3.Distance(transform.parent.position + warpPoints[warp], GameHandler.player.transform.position) > 5f) {
            this.transform.position = transform.parent.position + warpPoints[warp];
        }

        lastWarp = warp; 
        SpawnWarpEffect(); 
        SpawnProjectileCluster(); 
    }//RandomWarp

    private void Warp() {
        if (iterationTime >= iteration * WarpInterval) {
            warpTime = WarpSpinTime;  
            iteration++; 
        }

        if (iterationTime >= WarpBehaviorDuration) {
            currentBehavior = WizardBehavior.Wait;
            lastBehavior = WizardBehavior.Warp;
            iterationTime = 0;
            iteration = 0; 
        }
    }//Warp

    private void ReturnToStart() {
        SpawnWarpEffect();
        this.transform.position = startPos;
        SpawnWarpEffect();
    }//ReturnToStart

    private void SpawnWarpEffect() {
        if (warpEffect) {
            GameObject warp = (GameObject)GameObject.Instantiate(
                warpEffect,
                transform.position + new Vector3(0f, 1f),
                Quaternion.identity);
            warp.transform.parent = transform.parent;
        }
    }//SpawnWarpEffect

    //--------------------------------
    //Collision Functions
    //--------------------------------
    private void OnDeath() {
        ReturnToStart(); 
        if (damageable != null && !damageable.IsAlive()) {
            if (corpse != null) {
                GameObject newCorpse = (GameObject)GameObject.Instantiate(corpse, transform.position+new Vector3(0f,1f), Quaternion.identity);
                newCorpse.transform.parent = transform.parent;
            }
            GameHandler.PlayWorldAudio(deathSound);
            Destroy(gameObject);
        }
    }//HitByProjectile

    private void OnHit() {
        GameHandler.PlayWorldAudio(hitSound);
    }//OnHit

    private void OnDestroy() {
        //Remove Event Handlers
        damageable.OnDeath -= OnDeath;
    }//OnDestroy

    private void OnDrawGizmos() {
        Gizmos.color = new Color(1f,0f,1f);     
        if (warpPoints.Length != 0) {
            foreach (Vector3 warp in warpPoints) {
                Gizmos.DrawSphere(transform.parent.position + warp, 0.25f);
            }
        }
    }//OnDrawGizmos
}//Wizard
