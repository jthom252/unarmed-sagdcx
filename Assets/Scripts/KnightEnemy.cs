﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class KnightEnemy : MonoBehaviour {
    public float  moveSpeed = 8.5f;
    public float  aimingMoveSpeed = 5f; 
    public float  collisionStopDist = 0.4f; 
    public float telepathSeconds = 0.5f;
    public Hazardous.Friend friend = Hazardous.Friend.Friend_Player;

    private Animator  _animator; 

    private Damageable damageable;
    private float telepathSecondsRemaining;
    private bool isAttacking;
    private float attackSpeed;
    private float attackDuration = 1.0f;
    private float attackDurationRemaining;

    //public GameObject shieldObject; 
    //public Vector3 shieldBackPosition;
    //public Vector3 shieldBackRotation;
    //public Vector3 shieldAimPosition; 
    //public Vector3 shieldAimRotation; 

    public AudioClip hitSound; 
    public AudioClip deathSound; 
    
    public GameObject corpse;
    private NavMeshAgent navAgent;

    public bool isDead {
        get {
            if (damageable != null) {
                return !damageable.IsAlive();
            } 
            return false; 
        }
    }

	// Use this for initialization
	public void Start () {
        if (this.GetComponent<Animator>() == null) {
            throw new MissingComponentException("No animator attached to player.");
        }
        _animator = this.GetComponent<Animator>();

        damageable = GetComponent<Damageable>();
        if (damageable == null) {
            Debug.Log("Damageable component missing on player");
        }
        damageable.OnDeath += OnDeath; 
        damageable.OnHit += OnHit; 

        //shieldObject.transform.localPosition = shieldBackPosition; 
        //shieldObject.transform.localEulerAngles = shieldBackRotation;
        //shieldObject.SetActive(true); 
        
        attackSpeed = 2 * moveSpeed;

        navAgent = GetComponent<NavMeshAgent>();
	}//Start

    public void Update () {
        float distance = Mathf.Abs(Vector3.Distance(transform.position, GameHandler.player.transform.position));
        if (!isAttacking)
        {
            if (distance > collisionStopDist)
            {
                navAgent.Resume();
                telepathSecondsRemaining = telepathSeconds;
                _animator.SetFloat("WalkSpeed", moveSpeed);
                _animator.SetBool("Block", false);
                //shieldObject.transform.localPosition = shieldBackPosition;
                //shieldObject.transform.localEulerAngles = shieldBackRotation; 

                navAgent.destination = GameHandler.player.transform.position;
            } else {
                transform.LookAt(GameHandler.player.transform);
                navAgent.Stop();
                _animator.SetFloat("WalkSpeed", 0);
                _animator.SetBool("Block", true);
                //shieldObject.transform.localPosition = shieldAimPosition;
                //shieldObject.transform.localEulerAngles = shieldAimRotation; 
                telepathSecondsRemaining -= Time.deltaTime;

                if (telepathSecondsRemaining <= 0.0f)
                {
                    attackDurationRemaining = attackDuration;
                    isAttacking = true;
                }
            }
        } else {
            // ATTACK!
            if (attackDurationRemaining > 0.0f)
            {
                GetComponent<Rigidbody>().MovePosition(transform.position + transform.forward * Time.deltaTime * moveSpeed);
                _animator.SetFloat("WalkSpeed", attackSpeed);
                attackDurationRemaining -= Time.deltaTime;
            } else {
                isAttacking = false;
                telepathSecondsRemaining = telepathSeconds; 
            }
        }
    }//Update

    private void OnHit() {
        GameHandler.PlayWorldAudio(hitSound);
    }//OnHit

    private void OnDeath() {
        Debug.Log(damageable.HitPoints);
        if (damageable != null && !damageable.IsAlive())
        {
            if (corpse != null) {
                GameObject newCorpse = (GameObject)GameObject.Instantiate(corpse, transform.position, Quaternion.identity);
                newCorpse.transform.parent = transform.parent;
            }
            GameHandler.PlayWorldAudio(deathSound);
            Destroy(gameObject);
        }
    }//OnDeath

    private void OnCollisionEnter(Collision collision)
    {
        Damageable otherDamageable = collision.gameObject.GetComponent<Damageable>();
        if (otherDamageable != null && otherDamageable.friend != friend)
        {
            telepathSecondsRemaining = telepathSeconds * 2;
            isAttacking = false;
        }
    }

    private void OnDestroy() {
        //Remove Event Handlers
        damageable.OnDeath -= OnDeath;
        damageable.OnHit -= OnHit; 
    }//OnDestroy
}
