﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StandingTorch : MonoBehaviour {
    public GameObject activatedObject; 
    public bool       startActivated       = false;
    public bool       activateByProjectile = true; 
    public bool       dousable = true;
    private Elemental.Element element;
    private bool      _isActive;
    public bool       isActive {
        get {
            return _isActive; 
        }
    }
    public List<Interactable> interactionTargets; 

	private void Start () {
        if (GetComponentInChildren<ProjectilePassThrough>()) {
            GetComponentInChildren<ProjectilePassThrough>().ProjectileEnteredWithObject += OnProjectileEntered; 
        } else {
            throw new MissingComponentException("Torch has no projectile interaction. Add a ProjectilePassThrough object");
        }

        if (!GetComponent<Interactable>()) {
            this.gameObject.AddComponent<Interactable>(); 
        }
        GetComponent<Interactable>().Interact += OnInteract; 

        _isActive = startActivated; 
        if (activatedObject != null) activatedObject.SetActive(_isActive);

        element = GetComponent<Elemental>().element;
	}//Start

    private void OnInteract() {
        _isActive = GetComponent<Interactable>().activeState; 
        if (activatedObject != null) activatedObject.SetActive(_isActive);
        ExecuteInteractables(_isActive);
    }//Interact

    private void OnProjectileEntered(GameObject other) {
        Elemental otherElement = other.GetComponent<Elemental>();
        if (!_isActive)
        {
            if (otherElement != null)
            {
                if (activatedObject != null && activateByProjectile) {
                    if (otherElement.element == Elemental.Element.Element_Fire)
                    {
                        activatedObject.SetActive(true);
                        ExecuteInteractables();
                        element = otherElement.element;
                        _isActive = true;
                    }
                }
            }
        } else {
            if (otherElement == null)
            {
                otherElement = other.AddComponent<Elemental>();
                otherElement.element = element;
            } else if (otherElement.element == Elemental.Element.Element_Ice && dousable) {
                Destroy(other);
                activatedObject.SetActive(false);
                ExecuteInteractables(false);
                _isActive = false;
            }
        }
    }//OnProjectileEntered

    private void ExecuteInteractables(bool active = true) {
        foreach (Interactable interactable in interactionTargets) {
            if (interactable != null) interactable.Execute(active);
        }
    }//ExecuteInteractables

    private void OnDestroy() {
        if (GetComponentInChildren<ProjectilePassThrough>()) {
            GetComponentInChildren<ProjectilePassThrough>().ProjectileEnteredWithObject -= OnProjectileEntered; 
        }

        if (GetComponent<Interactable>()) {
            GetComponent<Interactable>().Interact -= OnInteract; 
        }
    }//OnDestroy

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        foreach (Interactable interactable in interactionTargets) {
            if (interactable != null) Gizmos.DrawLine(this.transform.position, interactable.transform.position);
        }
    }//OnDrawGizmos
}//StandingTorch
