﻿using UnityEngine;
using System.Collections;

public class SlimeEnemy : MonoBehaviour {
    private NavMeshAgent navAgent; 
    public GameObject corpse;

	public void Start () {
        if (this.GetComponent<Damageable>() == null) {
            this.gameObject.AddComponent<Damageable>();
        }
        this.GetComponent<Damageable>().HitByProjectile += HitByProjectile;
        this.GetComponent<Damageable>().OnDeath += OnDeath; 

        if (this.GetComponent<NavMeshAgent>() == null) {
            throw new MissingComponentException("Enemy has no NavMesh Agent"); 
        }
        navAgent = this.GetComponent<NavMeshAgent>();
	}//Start
	
	private void Update () {
        navAgent.SetDestination(GameHandler.player.transform.position);
	}//Update

    private void OnDeath() {
        if (corpse != null) {
            GameObject newCorpse = (GameObject)GameObject.Instantiate(corpse, transform.position, Quaternion.identity);
            newCorpse.transform.parent = transform.parent;
        }
        Destroy(this.gameObject);
    }//OnDeath

    private void HitByProjectile(Projectile projectile) {
        //Change my state
    }//HitByProjectile
}//SlimeEnemy
