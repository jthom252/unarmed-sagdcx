﻿using UnityEngine;
using System.Collections;

public class Elemental : MonoBehaviour {
    public GameHandler.GameEvent OnElementChange;

    public enum Element
    {
        Element_None,
        Element_Fire,
        Element_Ice,
    };

    public Element _element = Element.Element_Fire;
    public Element element {
        get {
            return _element; 
        }

        set {
            _element = value; 
            if (OnElementChange != null) OnElementChange(); 
        }
    }
}//Elemental
