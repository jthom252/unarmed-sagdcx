﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;

public class MainMenu : MonoBehaviour {
    public string levelToLoad;
    public CameraFocus camera; 
    public Vector3 mainCameraPos; 
    public Vector3 creditsCameraPos;

    private void Start() {
        InputHandler.Respawn += ControllerStart; 
        InputHandler.Pause += Quit; 
    }//Start

    private void Update() {
        if (Input.touchCount > 0) {
            LoadLevel(); 
        }

        if (Input.anyKey) {
            LoadLevel(); 
        }
    }//Update

    private void ControllerStart(bool pressed = true) {
        LoadLevel();
    }//ControllerStart

    private void Quit(bool pressed = true) {
        Application.Quit(); 
    }//ControllerStart

    public void Continue() {
        //Check if there's a PlayerPrefs object here
        LoadLevel(); 
    }//Continue

    public void LoadLevel() {
        Application.LoadLevel(levelToLoad); 
    }//OnClick

    public void LookAtCredits() {
        camera.focusTarget  = creditsCameraPos; 
    }//LookAtCredits

    public void LookAtMain() {
        camera.focusTarget  = mainCameraPos; 
    }//LookAtMain

    private void OnDestroy() {
        InputHandler.Respawn -= ControllerStart;
        InputHandler.Pause -= Quit; 
    }//OnDestroy
}//MainMenu
