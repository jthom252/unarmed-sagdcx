﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Damageable : MonoBehaviour {
    public int HitPoints;
    public bool isAlwaysInvulnerable;
    public bool ignoreTracking = false; 
    public float invulnerabilityPostHit = 2f; 
    public Hazardous.Friend friend = Hazardous.Friend.Friend_Enemy;

    private struct OriginalColor {
        public Material material; 
        public Color color;

        public OriginalColor(Color c, Material m) {
            color = c; 
            material = m;
        } 
    }

    private float invulnerableTime; 
    private List<OriginalColor> originalColors; 

    public delegate void OnHitByProjectile(Projectile projectile); 
    public event OnHitByProjectile HitByProjectile;

    //Adding this so the enemy death tracker can function
    public event GameHandler.GameEvent OnDeath;
    public event GameHandler.GameEvent OnHit; 

    public void OnCollisionEnter(Collision col) {
        Hazardous hazard = col.gameObject.GetComponent<Hazardous>();
        if (hazard != null) {
            if (friend != hazard.friend || friend == Hazardous.Friend.Friend_Player) {
                if (!isAlwaysInvulnerable && invulnerableTime <= 0f) DealDamage(hazard); 

                if (HitByProjectile != null && col.gameObject.GetComponent<Projectile>()) {
                    HitByProjectile(col.gameObject.GetComponent<Projectile>());
                } 
            }
        }
    }//OnCollisionEnter

    private void Start() {
        invulnerableTime = 0f;
        originalColors = new List<OriginalColor>();
        GetColor(); 
    }//Start

    private void Update() {
        if (isAlwaysInvulnerable) FlashColor(new Color(1f, 0.92f, 0f));

        if (invulnerableTime > 0f) {
            invulnerableTime -= Time.deltaTime;

            if (invulnerableTime % 0.2f <= 0.1f) {
                FlashColor(new Color(1f,0f,0f,0.2f));
            } else {
                FlashColor(new Color(0.5f,0f,0f,0.2f));
            }

            if (invulnerableTime <= 0f) {
                invulnerableTime = 0f;
                RevertColor();
            }
        }
    }//Update

    public int DealDamage(Hazardous hazard) {
        return DealDamage(hazard.Damage);
    }//DealDamage

    public int DealDamage(int damage) {
        if (!isAlwaysInvulnerable && invulnerableTime <= 0f) {
            HitPoints -= damage;
            Debug.Log(gameObject.name + " has " + HitPoints + " HP");

            GetColor();
            invulnerableTime = invulnerabilityPostHit;

            if (HitPoints <= 0 && OnDeath != null) {
                RevertColor();
                invulnerableTime = 0f;
                OnDeath();
            } else {
                if (OnHit != null) OnHit();
            }
        }

        return HitPoints; 
    }//DealDamage

    public bool IsAlive() {
        return HitPoints > 0;
    }//IsAlive

    //Probably see about changing this to a coroutine
    private List<Color> GetOriginalColors() {
        List<Color> colors = new List<Color>(); 
        if (GetComponentInChildren<MeshRenderer>()) {
            foreach (Material mat in GetComponentInChildren<MeshRenderer>().materials) {
                colors.Add(mat.color);
            }
        }
        if (GetComponentInChildren<SkinnedMeshRenderer>()) {
            foreach (Material mat in GetComponentInChildren<SkinnedMeshRenderer>().materials) {
                colors.Add(mat.color); 
            }
        }
        return colors; 
    }//GetOriginalColors

    public void GetColor() {
        if (GetComponent<MeshRenderer>()) {
            MeshRenderer m = GetComponent<MeshRenderer>();
            foreach (Material mat in m.materials) {
                originalColors.Add(new OriginalColor(mat.color, mat));
            }
        }

        if (GetComponentInChildren<MeshRenderer>()) {
            MeshRenderer m = GetComponentInChildren<MeshRenderer>();
            foreach (Material mat in m.materials) {
                originalColors.Add(new OriginalColor(mat.color, mat));
            }
        }

        if (GetComponentInChildren<SkinnedMeshRenderer>()) {
            SkinnedMeshRenderer sm = GetComponentInChildren<SkinnedMeshRenderer>();
            foreach (Material mat in sm.materials) {
                originalColors.Add(new OriginalColor(mat.color, mat));
            }
        }        
    }//GetColor

    public void RevertColor() {
        foreach (OriginalColor oc in originalColors) {
            oc.material.color = oc.color; 
        }
    }//RevertColor

    private void FlashColor(Color clr) {
        foreach (OriginalColor oc in originalColors) {
            oc.material.color = clr; 
        }
    }//FlashColor
}//Damageable
