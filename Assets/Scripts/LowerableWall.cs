﻿using UnityEngine;
using System.Collections;

public class LowerableWall : MonoBehaviour {
    private Interactable interactable; 
    public float loweringTime = 2f; 
    public float automaticRaiseTime = 6f; 
    public bool automaticallyRaise = true; 
    public bool automaticallyCycle = false; 

    private float lowerTime; 
    private bool lowered; 

    private void Start() {
        interactable = GetComponent<Interactable>(); 
        interactable.Interact += OnInteract;
    }//Start

    private void Update() {
        lowerTime += Time.deltaTime;
        if (lowered) {
            Lower(lowerTime/loweringTime);
        } else {
            Raise(lowerTime/loweringTime); 
        }

        if (lowered && lowerTime >= automaticRaiseTime && automaticallyRaise) {
            lowerTime = 0; 
            lowered = false;
        }

        if (automaticallyCycle && lowerTime >= automaticRaiseTime) {
            lowerTime = 0; 
            lowered = !lowered; 
        }
    }//Update

    private void Lower(float progress) {
        transform.localScale = new Vector3(1f,Mathf.Lerp(0.75f,0.05f,progress),1f);
    }//Lower

    private void Raise(float progress) {
        transform.localScale = new Vector3(1f, Mathf.Lerp(0.05f, 0.75f, progress), 1f);
    }//Raise

    private void OnInteract() {
        if (!lowered) {
            lowerTime = 0;
            lowered = true;
        }
    }//OnInteract

    private void OnDestroy() {
        if (interactable != null) interactable.Interact -= OnInteract; 
    }//OnDestroy
}//LowerableWall
