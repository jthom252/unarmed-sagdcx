﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class CameraFocus : MonoBehaviour {
    public bool           followPlayer = true; 
    public Vector3        focusTarget; 
    public AnimationCurve followCurve; 
    public float          followDelay = 0.2f; 
    public float          zOffset     = -20f; 

    private float         followTime;
    private Vector3       targetPosition;
    private Vector3       startPosition; 
    private bool          targetMoved;
    private float         focusTime; 

    private void Start() {
        followTime = 0; 
        focusTime  = 0; 
        targetMoved    = false;
    }//Start

	private void Update () {
        #if UNITY_EDITOR
        if (!UnityEditor.EditorApplication.isPlaying) {
            GameObject p = GameObject.Find("Player");
            if (p != null) TargetObject(p.transform.position, 1f);
        }
        #endif

        if (targetMoved) {
            followTime += Time.deltaTime; 
            if (followTime >= followDelay) targetMoved = false; 
        }

        if (!followPlayer) {
            TargetObject(focusTarget, followCurve.Evaluate(followTime / followDelay)); 
        } else {
            if (GameHandler.player != null) TargetObject(GameHandler.player.transform.position, followCurve.Evaluate(followTime / followDelay)); 
        }
	}//Update

    private void TargetObject(Vector3 target, float progress) {
        if (target != targetPosition) {
            targetMoved   = true;
            followTime    = 0f;
            startPosition = this.transform.position; 
        }
        targetPosition = target;
 
        Vector3 nextPos = new Vector3(
            Mathf.Lerp(startPosition.x, targetPosition.x, progress), 
            this.transform.position.y, 
            Mathf.Lerp(startPosition.z, targetPosition.z + zOffset, progress));
        transform.position = nextPos; 
    }//FocusObject
}//CameraFocus