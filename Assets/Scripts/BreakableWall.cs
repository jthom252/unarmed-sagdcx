﻿using UnityEngine;
using System.Collections;

public class BreakableWall : MonoBehaviour {
    public bool breakOnContact = false;
    public GameObject corpse; 
	private Damageable damageable;

	void Start() {
        damageable = GetComponent<Damageable>();
        if (damageable == null)
        {
            Debug.Log("Damageable component missing from BreakableWall - " + this.name);
        }
    }//Start

	void OnCollisionEnter(Collision collision) {
	    if (damageable != null && !damageable.IsAlive() || breakOnContact) {
            if (corpse != null) {
                GameObject newCorpse = (GameObject)GameObject.Instantiate(corpse, transform.position, Quaternion.identity);
                newCorpse.transform.parent = transform.parent;
            }
            Destroy(gameObject);
        }
    }//OnCollisionEnter
}//BreakableWall
