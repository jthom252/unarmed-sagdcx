﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class MusicPlayer : MonoBehaviour {
    public AudioSource soundPlayer; 

    public AudioClip   defaultMusicTrack;
    public AudioClip   bossMusicTrack;

    public void Mute(bool pressed = true) {
        if (soundPlayer && soundPlayer.isPlaying && pressed) soundPlayer.mute = !soundPlayer.mute;
    }//Mute

    public void PlayBossMusic() {
        if (bossMusicTrack != null) {
            soundPlayer.clip = bossMusicTrack;
            soundPlayer.Play();
            soundPlayer.loop = true;
        }
    }//PlayBossMusic

    public void PlayDefaultMusic() {
        if (defaultMusicTrack != null) {
            soundPlayer.clip = defaultMusicTrack;
            soundPlayer.Play();
            soundPlayer.loop = true;
        }
    }//PlayDefaultMusic

    private void Start() {
        PlayDefaultMusic();
        InputHandler.Mute += Mute; 
    }//Start

    private void PlaySound(AudioClip clip) {
        if (soundPlayer != null) {

        } else {
            Debug.LogError("There is no AudioSource attached to the Music Player or the audio clip was null"); 
        }
    }//PlaySound

    private void OnDestroy() {
        InputHandler.Mute -= Mute; 
    }
}//SoundHandler

