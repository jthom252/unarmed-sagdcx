﻿using UnityEngine;
using System.Collections;

public class ShieldCluster : MonoBehaviour {
	public float rotateSpeed = 3f; 
    private bool activated = false; 

	private void Update () {
	    transform.Rotate(new Vector3(0f,Time.deltaTime * rotateSpeed));
	}//Update

    public void Activate() {
        WizardShield[] shields = GetComponentsInChildren<WizardShield>();
        foreach (WizardShield shield in shields) {
            shield.GetComponent<MeshRenderer>().enabled = true; 
        }

        activated = true; 
    }//Activate
}//ShieldCluster
