﻿using UnityEngine;
using System.Collections;

public class SceneChange : MonoBehaviour {
    public string sceneToLoad = "";

    private void OnTriggerEnter(Collider col) {
        if (col.GetComponent<Player>()) {
            Fader.FadeOut(LoadScene); 
        }
    }//OnTriggerEnter

    private void LoadScene() {
        Application.LoadLevel(sceneToLoad); 
    }//LoadScene
}//SceneChange
