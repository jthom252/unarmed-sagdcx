﻿using UnityEngine;
using System.Collections;

public class SoundTrigger : MonoBehaviour {
    public AudioClip clip; 
    public bool playOnce = true;

	private void Start () {
        if (!GetComponent<Interactable>()) {
            throw new MissingComponentException("Sound trigger improperly configured");
        }
        GetComponent<Interactable>().Interact += OnInteract;
	}//Start

    private void OnInteract() {
        GameHandler.PlayWorldAudio(clip);
        if (playOnce) Destroy(gameObject);
    }//OnInteract
}//SoundTrigger
