﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour {
    //Use this for our various generic events
    public delegate void GameEvent();
    public delegate void GameEventWithObject(GameObject other);
    public static event GameEvent OnGamePaused; 
    public static event GameEvent OnGameUnpaused;

    private static GameHandler instance;

    public static Player player; 

    public static Text respawnText;
    public Room playerRespawnRoom; 
    public AudioSource audio; 


    private static Text pausedText;
    public static bool Paused;
    private static float timeScale;
    public static void PauseGame() {
        if (Time.timeScale > 0)
        {
            timeScale = Time.timeScale;
            Time.timeScale = 0;
            Paused = true;
            pausedText.enabled = true;
        } else {
            Time.timeScale = timeScale;
            Paused = false;
            pausedText.enabled = false;
        }
    }//PauseGame

    public static void PlayWorldAudio(AudioClip clip) {
        if (instance.audio != null && clip != null) instance.audio.PlayOneShot(clip); 
    }//PlayWorldAudio

    public static void StartRespawn(bool pressed = true) {
        if (player.isDead) Fader.FadeOut(RespawnPlayer);
    }//StartRespawn

    public static void PauseButton(bool pressed = true) {
        if (pressed) PauseGame(); 
    }//PauseGame

    public static void RespawnPlayer() {
        Debug.Log("Respawning Player");
        respawnText.enabled = false;
        player.transform.GetComponentInParent<Room>().OnRoomFled();
        instance.playerRespawnRoom.OnRoomEntered(); 
        player.transform.parent = instance.playerRespawnRoom.transform; 
        player.Respawn(instance.playerRespawnRoom.respawnPoint);
        Fader.FadeIn();
    }//GetRespawnPoint

    public static void ChangeRespawnRoom(Room room) {
        if (room != null && instance != null) instance.playerRespawnRoom = room;
    }//ChangeRespawnRoom

    //Regular Unity functions
    private void Start() {
        if (GameObject.Find("Player")) {
            player = GameObject.Find("Player").GetComponent<Player>();
        } else {
            throw new MissingComponentException("There is no Player object with the name 'Player'");
        }

        InputHandler.Respawn += StartRespawn;
        InputHandler.Pause   += PauseButton; 
        instance = this.GetComponent<GameHandler>(); 
        Fader.FadeIn(); 

        if (GameObject.Find("PausedText"))
        {
            pausedText = GameObject.Find("PausedText").GetComponent<Text>();
            pausedText.enabled = false;
        } else {
            throw new MissingComponentException("There is no PausedText object in the scene");
        }

        if (GameObject.Find("RespawnText"))
        {
            respawnText = GameObject.Find("RespawnText").GetComponent<Text>();
            respawnText.enabled = false;
        } else {
            throw new MissingComponentException("There is no RespawnText object in the scene");
        }

        if (GameObject.Find("HealthPanel"))
        {
            GameObject.Find("HealthPanel").GetComponent<HealthInterface>().Init();
        } else {
            throw new MissingComponentException("There is no HealthPanel object in the scene");
        }
    }//Start

    public void Update()
    {

        if (player.isDead && respawnText.enabled == false)
        {
            respawnText.enabled = true;
        }
    }

    private void OnDrawGizmos() {
        if (playerRespawnRoom != null) {
            Gizmos.color = Color.green; 
            Gizmos.DrawSphere(playerRespawnRoom.transform.position + playerRespawnRoom.respawnPoint, 0.25f); 
        }
    }//OnDrawGizmos

    private void OnDestroy() {
        InputHandler.Respawn -= StartRespawn;
        InputHandler.Pause -= PauseButton; 
    }//OnDestroy
}//GameHandler
