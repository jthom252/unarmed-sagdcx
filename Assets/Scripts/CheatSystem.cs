﻿using UnityEngine;
using System.Collections;

public static class CheatSystem {
    private const string GOD_MODE_CHEAT = "IAMGOD";
    private const string KEYS_CHEAT = "MASTEROFUNLOCKING"; 
    public const string KILL_CHEAT = "KILLEMALL";
    public const string SKIP_CHEAT = "SCREWTHIS";

    private static string cheatString;

    public static void AddToCheat(string s) {
        if (cheatString == null) {
            cheatString = s.ToUpper();
        } else {
            cheatString += s.ToUpper();
            if (cheatString.Length > 16) {
                cheatString.Remove(0,1); 
            }
        }
        Eval();
    }//AddToCheat

    private static void Eval() {
        if (cheatString.Contains(GOD_MODE_CHEAT)) {
            GameHandler.player.GetComponent<Damageable>().isAlwaysInvulnerable = !GameHandler.player.GetComponent<Damageable>().isAlwaysInvulnerable;
            cheatString = ""; 
        } else if (cheatString.Contains(KEYS_CHEAT)) {
            Key.AddKey(Key.KeyType.GoldKey); 
            Key.AddKey(Key.KeyType.SilverKey);
            cheatString = "";
        } else if (cheatString.Contains(KILL_CHEAT)) {
            Room r = GameHandler.player.GetComponentInParent<Room>(); 
            Damageable[] damageables = r.GetComponentsInChildren<Damageable>();
            foreach (Damageable d in damageables) {
                if (d.friend == Hazardous.Friend.Friend_Enemy || d.friend == Hazardous.Friend.Friend_None) d.DealDamage(1000);
            }
            cheatString = "";
        } else if (cheatString.Contains(SKIP_CHEAT)) {
            Room r = GameHandler.player.GetComponentInParent<Room>(); 
            r.OnRoomCompleted(); 
            cheatString = ""; 
        }
    }//Eval
}//Cheat
