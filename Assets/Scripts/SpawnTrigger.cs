﻿using UnityEngine;
using System.Collections;

public class SpawnTrigger : MonoBehaviour {
    public SpawnPoint[]   spawnObjects;
    private Interactable  interactable; 

    private void Start() {
        if (!GetComponent<Interactable>()) {
            gameObject.AddComponent<Interactable>(); 
        }
        interactable = GetComponent<Interactable>(); 
        interactable.Interact += OnInteract;
    }//Start

    private void OnInteract() {
        foreach (SpawnPoint spawn in spawnObjects) {
            spawn.Spawn(); 
        }
    }//OnInteract

    private void OnDrawGizmos() {
        foreach (SpawnPoint spawn in spawnObjects) {
            Gizmos.color = new Color(1f,0f,1f);  
            Gizmos.DrawLine(this.transform.position, spawn.transform.position);
        }
    }//OnDrawGizmos
}//SpawnTrigger
