﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class EnemyDefeatTracker : MonoBehaviour {
    private int enemiesToDefeat; 
    private EventTrigger trigger; 

	public void StartMonitoring () {
        enemiesToDefeat = 0; 

        if (this.GetComponent<EventTrigger>()) {
            trigger = this.GetComponent<EventTrigger>();
        } else {
            throw new MissingComponentException("A EnemyDefeatTracker is improperly configured (Has no EventTrigger)"); 
        }
        
        Damageable[] enemies = this.transform.parent.GetComponentsInChildren<Damageable>();
        foreach (Damageable enemy in enemies) {
            if (enemy.friend == Hazardous.Friend.Friend_Enemy && !enemy.ignoreTracking) {
                enemy.OnDeath += EnemyDefeated; 
                enemiesToDefeat++; 
            }
        }
        Debug.Log(enemiesToDefeat + "enemies are in this room.");

        if (enemiesToDefeat == 0) {
            Debug.LogWarning("An enemy defeat trigger is empty and will automatically trigger");
            trigger.Trigger();  
        }
	}//StartMonitoring

    //This needs to track the enemy that was defeated to remove the event handler
    private void EnemyDefeated() {
        --enemiesToDefeat;

        if (enemiesToDefeat <= 0) {
            trigger.Trigger(); 
        }
    }//EnemyDefeated
}//Start
