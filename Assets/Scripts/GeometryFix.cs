﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class GeometryFix : MonoBehaviour {
	void Update () {
	    Transform[] objs = this.GetComponentsInChildren<Transform>();

        foreach (Transform geom in objs) {
            Vector3 newVector = geom.localPosition; 
            Vector3 newRot    = geom.localEulerAngles; 

            newVector.x = Mathf.Round(newVector.x); 
            newVector.y = Mathf.Round(newVector.y);
            newVector.z = Mathf.Round(newVector.z);

            newRot.x    = Mathf.Round(newRot.x);
            newRot.y    = Mathf.Round(newRot.y);
            newRot.z    = Mathf.Round(newRot.z); 

            geom.localPosition = newVector; 
        }
	}//Update
}//GeometryFix
