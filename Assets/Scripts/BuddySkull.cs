﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;

public class BuddySkull : MonoBehaviour {
    private const string InteractTextKeyboard = "[E] TALK"; 
    private const string InteractTextJoystick = "[X] TALK";
    private const string InteractTextTouch    = "[TAP] TALK";
    private const string NextTextKeyboard = "\n[E] NEXT";
    private const string NextTextJoystick = "\n[X] NEXT"; 
    private const string NextTextTouch    = "\n[TAP] NEXT";

    public string[] defaultDisplayText; 
    public string[] interactDisplayText; 

    public float  timePerChar = 0.05f; 
    public bool   displayRandomText;
    public AudioClip talkSound;

    private bool   interactedWith; 
    private bool   canTalk; 
    private float  textDisplayTime; 
    private Canvas canvas; 
    private SpriteRenderer sprite; 
    private Text   text; 
    private bool   showText; 
    private int    charPos; 
    private int    arrayPos; 
    private string displayText;
    private string[] targetSource;

	void Start () {
        if (this.GetComponentInChildren<Canvas>()) {
            canvas = this.GetComponentInChildren<Canvas>();
        } else {
            throw new MissingComponentException("No Canvas on Skull"); 
        }

        if (canvas.GetComponentInChildren<Text>()) {
            text = canvas.GetComponentInChildren<Text>();
        } else {
            throw new MissingComponentException("No Text on Skull Canvas"); 
        }

        if (GetComponentInChildren<SpriteRenderer>()) {
            sprite = GetComponentInChildren<SpriteRenderer>();
        } else {
            throw new MissingComponentException("No Sprite on Skull Canvas");
        }

        interactedWith  = false; 
        textDisplayTime = 0f; 
        showText        = false; 
        canTalk         = true; 
        charPos         = 0;
        arrayPos        = -1; 
        canvas.gameObject.SetActive(false);
        sprite.gameObject.SetActive(false);

        if (GetComponent<Interactable>()) {
            GetComponent<Interactable>().Interact += OnInteract; 
        }

        InputHandler.Talk += OnTalk;
	}//Start

    private string PickRandomText() {
        return "Random";
    }//PickRandomText

    private void Update() {
        #if UNITY_EDITOR
        if (!UnityEditor.EditorApplication.isPlaying) {
            canvas.gameObject.SetActive(true);
            text.text = displayText;
        }
        #endif
        if (showText) {
        textDisplayTime += Time.deltaTime;
            if (textDisplayTime >= timePerChar) {
                if (charPos < displayText.Length) {
                    text.text += displayText[charPos];
                } else {
                    if ((arrayPos + 1) <= targetSource.Length-1) {
                        if (InputHandler.inputMode == InputHandler.InputMode.Joystick) {
                            text.text += NextTextJoystick;
                        } else if (InputHandler.inputMode == InputHandler.InputMode.Keyboard) {
                            text.text += NextTextKeyboard;
                        } else {
                            text.text += NextTextTouch;
                        }
                    }
                    showText = false; 
                }
                ++charPos; 
                GameHandler.PlayWorldAudio(talkSound);
                textDisplayTime = 0f + (textDisplayTime - timePerChar); 
            }
        }
    }//Update

    private void OnInteract() {
        interactedWith = true; 
        arrayPos = 0; 
    }//OnInteract

    private string PickText() {
        string returnText = "Random"; 

        if (interactedWith) {
            targetSource = interactDisplayText;
        } else {
            targetSource = defaultDisplayText; 
        }

        if (displayRandomText) {
            int val = Random.Range(0, targetSource.Length - 1);
            returnText = targetSource[val]; 
        } else {
            if (arrayPos < targetSource.Length - 1) {
                ++arrayPos; 
                returnText = targetSource[arrayPos]; 
            } else {
                sprite.gameObject.SetActive(false);
                if (InputHandler.inputMode == InputHandler.InputMode.Joystick) {
                    text.text = InteractTextJoystick;
                } else if (InputHandler.inputMode == InputHandler.InputMode.Touch) {
                    text.text = InteractTextKeyboard;
                } else {
                    text.text = InteractTextTouch; 
                }
                showText = false; 
                arrayPos = -1; 
            }
        }

        return returnText; 
    }//PickText

    private void OnTalk(bool pressed = true) {
        if (pressed && canTalk) {
            showText = true;
            charPos = 0;
            text.text = "";
            sprite.gameObject.SetActive(true);

            displayText = PickText();
        }
    }//OnTalk

    private void OnTriggerEnter(Collider col) {
        Debug.Log("Enter");
        if (col.gameObject.layer == LayerMask.NameToLayer("Player")) {
            arrayPos  = -1; 
            text.text = "";
            showText  = false; 
            canTalk   = true;

            if (InputHandler.inputMode == InputHandler.InputMode.Joystick) {
                text.text = InteractTextJoystick;
            } else if (InputHandler.inputMode == InputHandler.InputMode.Keyboard) {
                text.text = InteractTextKeyboard;
            } else {
                text.text = InteractTextTouch;
            }

            canvas.gameObject.SetActive(true);
            sprite.gameObject.SetActive(false);
        }
    }//OnTriggerEnter

    private void OnTriggerExit(Collider col) {
        if (col.gameObject.layer == LayerMask.NameToLayer("Player")) {
            showText = false;
            canTalk  = false; 
            canvas.gameObject.SetActive(false);
            sprite.gameObject.SetActive(false);
        }
    }//OnTriggerExit
}//BuddySkull
