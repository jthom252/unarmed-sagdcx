﻿using UnityEngine;
using System.Collections;

public class RoomCreator : MonoBehaviour {
    public int width;
    public int height;

    public GameObject Wall;
    public GameObject Floor;

    public float PlayerX;
    public float PlayerZ;
    public GameObject Player;

	// Use this for initialization
	void Start () {
        // Transform is center of the room, so we need to move to the top-left,
        // from here we move like a type-writter to build the room
        float startX = transform.position.x - (width / 2);
        float startY = transform.position.z - (height / 2);

        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                Vector3 newPos = new Vector3((float) x + startX, 0.0f, (float) y + startY);
                Instantiate(Floor, newPos, Quaternion.identity);
                if (x == 0 || y == 0 || x == (width - 1) || y == (height - 1))
                {
                    Instantiate(Wall, newPos, Quaternion.identity);
                }
            }
        }

        GameObject player = (GameObject) Instantiate(Player, new Vector3(PlayerX, 0.0f, PlayerZ), Quaternion.identity);
        GameObject mainCamera = (GameObject) GameObject.FindWithTag("MainCamera");
        if (mainCamera != null)
        {
            CameraFocus cameraFocus = mainCamera.GetComponent<CameraFocus>();
            //cameraFocus.followTarget = player;
        }
	}
}
