﻿using UnityEngine;
using System.Collections;

public class HomingSkull : MonoBehaviour {
    private NavMeshAgent navAgent;
    public float timeToLive;
    public GameObject corpse;

    private float lifeTime; 
    private Damageable damageable; 

    public void Start() {
        if (this.GetComponent<NavMeshAgent>() == null) {
            throw new MissingComponentException("Enemy has no NavMesh Agent");
        }
        navAgent = this.GetComponent<NavMeshAgent>();

        if (this.GetComponent<Damageable>() == null) {
            throw new MissingComponentException("Enemy has no NavMesh Agent");
        }
        damageable = this.GetComponent<Damageable>();
        damageable.OnDeath += OnDeath; 

        lifeTime = 0f; 
        Explode();
    }//Start

    private void Update() {
        navAgent.SetDestination(GameHandler.player.transform.position);

        lifeTime += Time.deltaTime;
        if (lifeTime >= timeToLive) {
            Explode(); 
            OnDeath();
        }
    }//Update

    private void OnDeath() {
        Explode(); 
        Destroy(gameObject);
    }//OnDeath

    private void Explode(bool parentSelf = false) {
        if (corpse != null) {
            GameObject newCorpse = (GameObject)GameObject.Instantiate(corpse, transform.position, Quaternion.identity);
            if (!parentSelf) newCorpse.transform.parent = transform.parent;
        }
    }//Explode

    private void OnCollisionEnter(Collision col) {
        if (col.gameObject.layer == Top.LAYER_PLAYER) OnDeath();
    }//OnCollisionEnter

    private void OnDestroy() {
        damageable.OnDeath -= OnDeath; 
    }//OnDestroy
}//HomingSkull
