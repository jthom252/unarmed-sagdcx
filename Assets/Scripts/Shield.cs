﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {
    public bool canBlock = true;

    protected void OnTriggerEnter(Collider col) {
        Debug.Log("ShieldEnter" + transform.parent.name);
        if (col.GetComponent<Projectile>() && canBlock) {
            Vector3 forward  = this.transform.forward; 
            float xAngle = forward.x;
            float zAngle = forward.z; 
            float rotation = Mathf.Atan2(zAngle, xAngle) * 180f / Mathf.PI; 

            col.transform.localEulerAngles = new Vector3(col.transform.localEulerAngles.x,
                rotation,
                col.transform.localEulerAngles.z); 
            Hazardous otherHazard = col.gameObject.GetComponent<Hazardous>();
            Damageable thisDamageable = gameObject.GetComponentInParent<Damageable>();
            if (otherHazard != null && thisDamageable != null && col.gameObject.GetComponent<Projectile>() != null)
            {
                col.gameObject.GetComponent<Projectile>().Explode(false);
                otherHazard.friend = thisDamageable.friend;
            }
        }

        if (GetComponentInParent<Player>() && col.GetComponent<Knockback>() && canBlock) {
            GetComponentInParent<Player>().Knockback(col.GetComponent<Knockback>(), col.transform.position);
        }
    }//OnTriggerEnter
}//Shield
