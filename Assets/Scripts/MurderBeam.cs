﻿using UnityEngine;
using System.Collections;

public class MurderBeam : MonoBehaviour {
    public float moveSpeed = 3.5f; 
    public float timeToLive;
    private float lifeTime;

    public void Start() {

        lifeTime = 0f;
    }//Start

    private void Update() {
        float rotation = this.transform.localEulerAngles.y * Mathf.PI / 180f;
        Vector3 nextPos = new Vector3(this.transform.position.x - (Mathf.Cos(rotation) * Time.deltaTime * moveSpeed),
            this.transform.position.y,
            this.transform.position.z - (Mathf.Sin(rotation) * Time.deltaTime * moveSpeed));

        Vector3 direction = this.transform.position - nextPos;

        RaycastHit[] hits = Physics.RaycastAll(this.transform.position, -direction.normalized, 0.5f, 1 << LayerMask.NameToLayer("Wall"));
        Debug.DrawRay(this.transform.position, -direction.normalized, Color.red);

        if (hits.Length > 0) {
            Destroy(gameObject);
        }

        transform.position = nextPos; 

        lifeTime += Time.deltaTime;
        if (lifeTime >= timeToLive) {
            Destroy(gameObject);
        }
    }//Update

    private void OnTriggerEnter(Collider col) {
        if (col.gameObject.GetComponent<Player>()) {
            col.gameObject.GetComponent<Damageable>().DealDamage(this.GetComponent<Hazardous>()); 
        }
    }//OnTriggerEnter
}//MurderBeam
