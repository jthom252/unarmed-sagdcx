﻿using UnityEngine;
using System.Collections;

public class RoomTransition : MonoBehaviour {
    public RoomTransition targetTransition; 
    public Vector3        exitPoint; 

    public delegate void TransitEvent(); 
    public event TransitEvent TransitionPointExit; 
    public event TransitEvent TransitionPointEnter;

    private void OnTriggerEnter(Collider col) {
        if (col.gameObject.name == "Player") {
            col.gameObject.transform.position = new Vector3(targetTransition.transform.position.x + targetTransition.exitPoint.x,
                col.gameObject.transform.position.y,
                targetTransition.transform.position.z + targetTransition.exitPoint.z);
            col.gameObject.transform.parent = targetTransition.transform.parent; 
            
            //Process events here
            if (TransitionPointExit != null) {
                TransitionPointExit(); 
            }

            if (targetTransition.TransitionPointEnter != null) {
                targetTransition.TransitionPointEnter(); 
            }
        }
    }//OnTriggerEnter

    private void OnDrawGizmos() {
        Gizmos.color = new Color(0f,0f,1f,0.5f);
        Gizmos.DrawWireSphere(this.transform.position, 0.33f); 
        Gizmos.color = new Color(1f,0f,0f,0.5f); 
        Gizmos.DrawWireSphere(this.transform.position + exitPoint, 0.33f); 
        Gizmos.color = new Color(0f,0f,1f);

        if (targetTransition != null) {
            Gizmos.DrawLine(this.transform.position, targetTransition.transform.position);
            Gizmos.DrawLine(targetTransition.transform.position, targetTransition.transform.position - new Vector3(0.5f, 0f) + (this.transform.position - targetTransition.transform.position) * 0.4f);
            Gizmos.DrawLine(targetTransition.transform.position, targetTransition.transform.position + new Vector3(0.5f, 0f) + (this.transform.position - targetTransition.transform.position) * 0.4f);
        }
    }//OnDrawGizmos
}//RoomTransition
