﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using UnityEngine.UI; 

public class Player : MonoBehaviour {
    public float  moveSpeed = 8.5f;
    public float  aimingMoveSpeed = 5f; 
    public float  collisionStopDist = 0.4f; 
    public Hazardous.Friend friend = Hazardous.Friend.Friend_Player;

    private bool      movingLeft; 
    private bool      movingRight;
    private bool      movingUp; 
    private bool      movingDown; 
    private bool      isAiming; 
    private bool      holdingObject; 
    private SpriteRenderer aimIndicator; 
    private Animator  _animator; 
    private List<GameObject> hiddenWalls; 
    private Vector3    lastPosition; 
    private Damageable damageable;

    public Shield shieldObject; 
    public Vector3 shieldBackPosition;
    public Vector3 shieldBackRotation;
    public Vector3 shieldAimPosition; 
    public Vector3 shieldAimRotation; 
    public GameObject spawnOnDeath; 
    public GameObject corpse; 

    public AudioClip hitSound;
    public AudioClip deathSound;

    //Various events
    public GameHandler.GameEvent PlayerDied;
    public GameHandler.GameEvent PlayerRespawned; 

    public bool isDead {
        get {
            if (damageable != null) {
                return !damageable.IsAlive();
            } 
            return false; 
        }
    }

    public bool hasShield {
        get {
            if (shieldObject == null) {
                return false;
            } else {
                return true; 
            }
        }
    }

#if UNITY_EDITOR
    private Text      debugText; 
#endif

	public void Start () {
        movingLeft  = false; 
        movingRight = false; 
        movingUp    = false; 
        movingDown  = false; 
        isAiming    = false; 
        holdingObject = false; 

        if (this.GetComponent<Animator>() == null) {
            throw new MissingComponentException("No animator attached to player.");
        }
        _animator = this.GetComponent<Animator>();

        damageable = GetComponent<Damageable>();
        if (damageable == null) {
            Debug.Log("Damageable component missing on player");
        }
        damageable.OnHit           += OnHit; 
        damageable.OnDeath         += OnDeath;

        InputHandler.Aim       += Aim; 
        InputHandler.Skip      += PlayerSkipRoom; 
        InputHandler.Cheat     += Cheat; 

        aimIndicator = this.GetComponentInChildren<SpriteRenderer>();
        hiddenWalls = new List<GameObject>();

        if (shieldObject != null) {
            shieldObject.transform.localPosition = shieldBackPosition;
            shieldObject.transform.localEulerAngles = shieldBackRotation;
            shieldObject.canBlock = false;
            shieldObject.gameObject.SetActive(false);
        }

        lastPosition = this.transform.position; 

        if (GameObject.Find("DebugText")) {
        #if UNITY_EDITOR
            debugText = GameObject.Find("DebugText").GetComponent<Text>();
        #else
            GameObject.Find("DebugText").GetComponent<Text>().enabled = false;
        #endif
        } else {
            Debug.LogWarning("Debug UI Disabled");
        }
	}//Start

	public void Update () {
        if (!GameHandler.Paused) {
            Rigidbody rBody = this.GetComponent<Rigidbody>();
            float speed = moveSpeed;
            if (isAiming || holdingObject) speed = aimingMoveSpeed;

            Vector3 nextPos = new Vector3((speed * Time.deltaTime * InputHandler.GetXMovement()) + transform.position.x,
                transform.position.y,
                (speed * Time.deltaTime * InputHandler.GetZMovement()) + transform.position.z);

            Vector3 currPos = this.transform.position;
            _animator.SetFloat("WalkSpeed", (Mathf.Abs(InputHandler.GetXMovement()) + Mathf.Abs(InputHandler.GetZMovement())) * speed);

            Ray moveRay = new Ray(currPos, nextPos - currPos);
            RaycastHit[] moveHits = Physics.RaycastAll(moveRay, collisionStopDist, 1 << LayerMask.NameToLayer("Wall"));

            if (rBody.velocity.x != 0 && rBody.velocity.z != 0) {
                Debug.DrawRay(lastPosition, currPos - lastPosition, Color.white, 100f);
                moveRay = new Ray(lastPosition, currPos - lastPosition);
                RaycastHit[] physicHits = Physics.RaycastAll(moveRay, collisionStopDist, 1 << LayerMask.NameToLayer("Wall"));
                if (physicHits.Length > 0) {
                    nextPos = lastPosition;
                }
            }

            if (moveHits.Length == 0) {
                this.transform.position = nextPos;
                lastPosition = nextPos;
            }

            if (isAiming && hasShield) {
                //Use mouse aiming if the aim key is held
                transform.localEulerAngles = new Vector3(0f, -InputHandler.GetMouseRotation());
            } else {
                transform.localEulerAngles = new Vector3(0f, InputHandler.controllerAngle);
            }

            #if UNITY_EDITOR
            if (debugText != null) {
                debugText.text =
                    "MovementX: " + InputHandler.GetXMovement() +
                    "\nMovementZ: " + InputHandler.GetZMovement() +
                    "\nIsAiming: " + isAiming +
                    "\nPlayerAngle: " + this.transform.localEulerAngles +
                    "\nPlayerAlive: " + damageable.IsAlive() +
                    "\nPlayerHP: " + damageable.HitPoints;
            }
            #endif
        }
	}//Update

    //--------------------------------
    //Shield Functions
    //--------------------------------
    public void GiveShield(GameObject newShield = null) {
        if (newShield != null) {
            GameObject instantiatedShield = (GameObject)GameObject.Instantiate(
                newShield,
                new Vector3(0f,0f),
                Quaternion.identity);

            instantiatedShield.transform.parent = this.transform; 
            if (instantiatedShield != null && instantiatedShield.GetComponent<Shield>()) {
                if (hasShield) Destroy(shieldObject.gameObject);
                Debug.Log("Setting new shield");
                shieldObject = instantiatedShield.GetComponent<Shield>();
                shieldObject.gameObject.SetActive(true);
                shieldObject.transform.position = shieldBackPosition;
                shieldObject.transform.localEulerAngles = shieldBackRotation;
            }
        }
    }//GiveShield

    private void DropShield() {
        Debug.Log("Dropping Shield");
        GameObject newShield = (GameObject)GameObject.Instantiate(
            spawnOnDeath,
            new Vector3(this.transform.position.x,
                this.transform.position.y + 1f,
                this.transform.position.z),
            Quaternion.identity);
        newShield.GetComponent<ShieldPickup>().SetShield((GameObject)GameObject.Instantiate(shieldObject.gameObject));
        newShield.transform.parent = this.transform.parent;
        Destroy(shieldObject.gameObject);
    }//DropShield

    //--------------------------------
    //Respawning & Death
    //--------------------------------
    public void Respawn(Vector3 newPos) {
        this.gameObject.SetActive(true);
        this.GetComponent<Rigidbody>().velocity = new Vector3(0f,0f,0f);

        this.transform.localPosition = newPos; 
        damageable.HitPoints    = 3; 
    }//Respawn

    private void OnHit() {
        GameHandler.PlayWorldAudio(hitSound);
        Debug.Log(damageable.HitPoints);
    }//OnHit

    private void OnDeath() {
        if (spawnOnDeath && hasShield) {
            //DropShield();
        }
        isAiming = false; 
        aimIndicator.enabled = false; 

        GameObject spawnedCorpse = (GameObject)GameObject.Instantiate(corpse,
        transform.position + new Vector3(0f, 1f, 0f),
        Quaternion.identity);
        spawnedCorpse.transform.parent = transform.parent.transform;

        GameHandler.PlayWorldAudio(deathSound);

        if (PlayerDied != null) PlayerDied();
        this.gameObject.SetActive(false);
    }//OnDeath

    //--------------------------------
    //General Functions
    //--------------------------------
    public void Knockback(Knockback knock, Vector3 pos) {
        if (knock.canKnockback) {

            float xAngle = pos.x - this.transform.position.x;
            float zAngle = pos.z - this.transform.position.z;
            float angle = Mathf.Atan2(zAngle, xAngle);

            this.GetComponent<Rigidbody>().AddForce(new Vector3(
                -Mathf.Cos(angle) * knock.forceAmount,
                0f,
                -Mathf.Sin(angle) * knock.forceAmount));
        }
    }//Knockback

    private void HideWalls() {
        //Hidden walls -- Optimize this stuff eventually, probably bad performance wise. 
        //Test the areas to the left and right of the player
        List<RaycastHit> hits = new List<RaycastHit>();
        for (int i = -10; i < 20; ++i) {
            RaycastHit[] iterateHits = Physics.RaycastAll(this.transform.position,
                new Vector3(-1f + (0.1f * i), 0, -1f),
                10f,
                1 << LayerMask.NameToLayer("Wall"));
            for (int j = 0; j < iterateHits.Length; ++j) {
                hits.Add(iterateHits[j]);
            }
        }

        //Check if the wall is in the array already, if turn off the graphics
        for (int i = 0; i < hits.Count; ++i) {
            GameObject obj = hits[i].transform.gameObject;
            if (obj.tag == "CanBeHidden") {
                hits[i].transform.gameObject.GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f, hits[i].distance / 10f);
                if (!hiddenWalls.Contains(obj)) {
                    hiddenWalls.Add(obj);
                }
            }
        }

        //If the wall is no longer hit then bring it back to normal
        for (int i = 0; i < hiddenWalls.Count; ++i) {
            bool isHit = false;
            for (int j = 0; j < hits.Count; ++j) {
                if (hits[j].transform.gameObject == hiddenWalls[i]) isHit = true;
            }
            if (!isHit) {
                hiddenWalls[i].GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f, 1f);
                hiddenWalls.Remove(hiddenWalls[i]);
            }
        }
    }//HideWalls

    private void OnCollisionEnter(Collision col) {
        if (col.gameObject.GetComponent<Knockback>()) {
            Knockback(col.gameObject.GetComponent<Knockback>(), col.transform.position);
        } 
    }//OnCollisionEnter

    //--------------------------------
    //Input 
    //--------------------------------
    private void PlayerSkipRoom(bool pressed = true) {
        #if UNITY_EDITOR
        if (damageable.IsAlive()) this.GetComponentInParent<Room>().OnRoomCompleted(); 
        #endif
    }//PlayerSkipRoom

    private void Cheat(bool pressed = true) {
        #if UNITY_EDITOR
        damageable.isAlwaysInvulnerable = true; 
        #endif
    }//MoveLeft

    private void Aim(bool pressed = true) {
        if (!GameHandler.Paused) {
            isAiming = pressed;
            _animator.SetBool("Block", pressed);

            if (isAiming && hasShield) {
                aimIndicator.enabled = true;
                shieldObject.canBlock = true;
            } else if (hasShield) {
                aimIndicator.enabled = false;
                shieldObject.canBlock = false;
            }

            if (hasShield) {
                if (pressed) {
                    shieldObject.transform.localPosition = shieldAimPosition;
                    shieldObject.transform.localEulerAngles = shieldAimRotation;
                } else {
                    shieldObject.transform.localPosition = shieldBackPosition;
                    shieldObject.transform.localEulerAngles = shieldBackRotation;
                }
            }
        }
    }//Aim

    //--------------------------------
    //Cleanup
    //--------------------------------
    private void OnDestroy() {
        //Remove Event Handlers
        damageable.OnHit -= OnHit;
        InputHandler.Aim       -= Aim;
        InputHandler.Skip      -= PlayerSkipRoom;
    }//OnDestroy
}//Player
