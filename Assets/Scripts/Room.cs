﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class Room : MonoBehaviour {
    //We can use this if we want special tracking later
    public enum RoomState {
        NotEntered,
        Fled,
        Completed,
        AlwaysRespawn
    }
    public RoomState currentState = RoomState.NotEntered; 
    public GameObject completionTrigger;  
    public Vector3    respawnPoint; 
    public AudioClip  roomCompleteSound;
    public bool       setAsRespawnCheckpoint;

    public bool       adjustCamera = false;
    public float      cameraAdjustHeight = 0f; 
    public float      cameraAdjustOffset = 0f; 
    public float      cameraRotationAdjust = 0f; 

    public Interactable[] activateOnComplete;
    public Interactable[] deactivateOnComplete; 

    public void Start() {
        RoomTransition[] transitPoints = this.GetComponentsInChildren<RoomTransition>();

        for (int i = 0; i < transitPoints.Length; ++i) {
            transitPoints[i].TransitionPointEnter += OnRoomEntered; 
            transitPoints[i].TransitionPointExit  += OnRoomExit; 
        }

        //Check if the completion handler exists and assign a event handler
        if (completionTrigger != null && completionTrigger.GetComponent<EventTrigger>()) {
            completionTrigger.GetComponent<EventTrigger>().OnTriggered += OnRoomCompleted; 
        } else {
            Debug.LogWarning("A room has no completion trigger assigned. Will automatically be set to Completed state. Ignore if you intended this!");
            roomCompleteSound = null; 
            OnRoomCompleted(); 
        }

        //Disable the display and other active properties for this room if the player isn't here
        if (this.GetComponentInChildren<Player>() == null) {
            gameObject.SetActive(false);
        } else {
            OnRoomEntered(); 
        }
    }//Start

    public void OnRoomEntered() {
        //Process all the enabling of objects
        gameObject.SetActive(true);

        if (setAsRespawnCheckpoint) GameHandler.ChangeRespawnRoom(this); 

        if (currentState != RoomState.Completed) {
            SpawnPoint[] spawnPoints = this.GetComponentsInChildren<SpawnPoint>();
            foreach (SpawnPoint point in spawnPoints) {
                if (point.behavior == SpawnPoint.SpawnBehavior.OnRoomEnter) {
                    point.Spawn();
                    Debug.Log("Spawning");
                }
            }

            Door[] doors = this.GetComponentsInChildren<Door>();
            foreach (Door door in doors) {
                if (door.behavior == Door.DoorBehavior.OpenOnRoomComplete) door.Close(); 
            }

            //Enemy trackers need to be started since they spawn in on room entered
            EnemyDefeatTracker[] enemyTrackers = this.GetComponentsInChildren<EnemyDefeatTracker>();
            foreach (EnemyDefeatTracker eTracker in enemyTrackers) {
                eTracker.StartMonitoring(); 
            }
        }

        if (adjustCamera) {
            CameraFocus localCam = GameObject.FindObjectOfType<CameraFocus>(); 
            localCam.transform.position += new Vector3(0f,cameraAdjustHeight,0f);
            localCam.zOffset += cameraAdjustOffset; 
            localCam.transform.Rotate(new Vector3(cameraRotationAdjust,0f)); 
        }
    }//OnRoomEntered

    public void OnRoomExit() {
        //Process the deletion and disabling of objects
        Cleanup[] cleanupObjects = this.GetComponentsInChildren<Cleanup>();
        foreach (Cleanup clean in cleanupObjects) {
            clean.Process(); 
        }

        if (adjustCamera) {
            CameraFocus localCam = GameObject.FindObjectOfType<CameraFocus>();
            localCam.transform.position -= new Vector3(0f, cameraAdjustHeight, 0f);
            localCam.zOffset -= cameraAdjustOffset;
            localCam.transform.Rotate(new Vector3(-cameraRotationAdjust, 0f)); 
        }

        gameObject.SetActive(false); 
    }//OnRoomExit

    public void OnRoomCompleted() {
        currentState = RoomState.Completed; 

        if (roomCompleteSound != null) GameHandler.PlayWorldAudio(roomCompleteSound);
        SpawnPoint[] spawnPoints = this.GetComponentsInChildren<SpawnPoint>();
        foreach (SpawnPoint point in spawnPoints) {
            if (point.behavior == SpawnPoint.SpawnBehavior.OnRoomComplete) {
                point.Spawn();
                Debug.Log("Spawning");
            }
        }

        Door[] doors = this.GetComponentsInChildren<Door>();
        Debug.Log(doors.Length + " in this room being opened.");
        foreach (Door door in doors) {
            if (door.behavior == Door.DoorBehavior.OpenOnRoomComplete) door.Open();
        }

        if (completionTrigger != null) {
            completionTrigger.GetComponent<EventTrigger>().OnTriggered -= OnRoomCompleted;
        }

        if (activateOnComplete.Length > 0) {
            foreach (Interactable interact in activateOnComplete) {
                if (interact != null) interact.Execute(true);
            }
        }

        if (deactivateOnComplete.Length > 0) {
            foreach (Interactable interact in deactivateOnComplete) {
                if (interact != null) interact.Execute(false);
            }
        }
    }//OnRoomCompleted

    //Use this if the player dies
    public void OnRoomFled() {
        if (currentState != RoomState.Completed) currentState = RoomState.Fled;
        OnRoomExit(); 
    }//OnRoomFled

    private void OnDrawGizmos() {
        Gizmos.color = new Color(1f,0.8f,0.2f,0.5f); 
        Gizmos.DrawSphere(this.transform.position + respawnPoint, 0.5f);

        Gizmos.color = Color.yellow; 
        if (activateOnComplete != null && activateOnComplete.Length > 0) {
            foreach (Interactable interact in activateOnComplete) {
                Gizmos.DrawLine(this.transform.position, interact.transform.position); 
            }
        }

        if (deactivateOnComplete != null && deactivateOnComplete.Length > 0) {
            foreach (Interactable interact in deactivateOnComplete) {
                if (interact != null) Gizmos.DrawLine(this.transform.position, interact.transform.position); 
            }
        }
    }//OnDrawGizmos
}//Room
