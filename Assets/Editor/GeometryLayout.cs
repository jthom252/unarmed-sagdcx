﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class GeometryLayout : EditorWindow {
    private static GeometryLayout window; 
    public float yPos; 

    [MenuItem("Custom Tools/Geometry Layout &G")]
    private static void Init() {
        window = GetWindow<GeometryLayout>(); 
        window.Show();
    }//Init

    private void OnSceneGUI(SceneView scene) {
        Vector2 mPos = Event.current.mousePosition;

        Handles.BeginGUI();
        Handles.color = Color.yellow;
        Handles.DrawCamera(new Rect(0f, 0f, 2000f, 2000f), scene.camera);
        Handles.Label(HandleUtility.GUIPointToWorldRay(new Vector2()).GetPoint(2f),
            "ScreenPosX " + scene.camera.transform.position.x +
            "\nScreenPosY " + scene.camera.transform.position.y +
            "\nScreenPosZ " + scene.camera.transform.position.z);
        Draw3DBox(HandleUtility.GUIPointToWorldRay(mPos).GetPoint(10f));
        Handles.EndGUI();

        GetWindow<GeometryLayout>().Repaint();
    }//OnSceneGUI

    private void Draw3DBox(Vector3 start) {
        start.x = Mathf.Round(start.x);
        start.y = yPos;
        start.z = Mathf.Round(start.z);

        if (Event.current.type == EventType.mouseDown && Event.current.shift && Selection.activeGameObject != null) {
            Debug.Log("Create object at position");
            GameObject newObj = (GameObject)Instantiate(Selection.activeGameObject, start, Quaternion.identity);
            newObj.name = Selection.activeGameObject.name;
        }

        Handles.DrawLine(start, start + new Vector3(-1f, 0f));
        Handles.DrawLine(start + new Vector3(-1f, 0f), start + new Vector3(-1f, 0f, -1f));
        Handles.DrawLine(start + new Vector3(-1f, 0f, -1f), start + new Vector3(0f, 0f, -1f));
        Handles.DrawLine(start + new Vector3(0f, 0f, -1f), start);
    }//Draw3DBox

    private void Update() {
        SceneView.RepaintAll(); 
    }//Update

    private void OnGUI() {
        EditorGUILayout.BeginVertical(); 
        EditorGUILayout.FloatField("Y Position", yPos, GUILayout.Width(200f), GUILayout.Height(20f));

        if (window == null) {
            window = GetWindow<GeometryLayout>(); 
        }

        Rect buttonRect = new Rect(0f,
            window.position.height - 64f,
            window.position.width,
            64f);
        if (SceneView.onSceneGUIDelegate != OnSceneGUI) {
            if (GUI.Button(buttonRect, "Modify")) {
                SceneView.onSceneGUIDelegate += OnSceneGUI;
            }
        } else {
            if (GUI.Button(buttonRect, "Stop")) {
                SceneView.onSceneGUIDelegate -= OnSceneGUI;
            }
        }
        EditorGUILayout.EndVertical(); 
    }//OnGUI

    private void OnDestroy() {
        if (SceneView.onSceneGUIDelegate == OnSceneGUI) {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }
    }//Close
}//GeometryLayout
